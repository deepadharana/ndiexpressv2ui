import { RouterModule, Routes } from '@angular/router';
import { NgModule, ModuleWithProviders } from '@angular/core/';
import { AppComponent } from "./components/app.component";
import { LoginComponent } from "./components/login/login.component";
import { ResetPasswordComponent } from "./components/reset-password/reset-password.component";
import { CustomerRegistrationComponent } from "./components/customer-registration/customer-registration.component";
import { ChoosePasswordComponent } from "./components/choose-password/choose-password.component";
import { LandingComponent } from "./components/landing/landing.component";
import { ChangePasswordComponent } from "./components/change-password/change-password.component";
import { ServiceRequestComponent } from "./components/service-request/service-request.component";
import { MyAccountComponent } from "./components/my-account/my-account.component";
import { AccountDetailsComponent } from "./components/account-details/account-details.component";
import { SalesDashboardComponent } from "./components/sales-dashboard/sales-dashboard.component";
import { ComingSoonComponent } from "./components/coming-soon/coming-soon.component";
import { ItemHistoryComponent } from "./components/item-history/item-history.component";
import { FavouritesComponent } from "./components/favourites/favourites.component";
import { SearchCustomerComponent } from "./components/search-customer/search-customer.component";
import { UserMaintenanceComponent } from "./components/user-maintenance/user-maintenance.component";
import { CustomerRequestComponent } from "./components/customer-request/customer-request.component";
import { AccountManagementComponent } from "./components/account-management/account-management.component";
import { ItemDetailComponent } from "./components/item-detail/item-detail.component";
import { SavedOrdersComponent } from "./components/saved-orders/saved-orders.component";
import { ItemSearchComponent } from "./components/item-search/item-search.component";

const routes : Routes=[        
        {path : 'login',component : LoginComponent},
        {path : 'home',component : LandingComponent},    
        {path : 'reset-password',component : ResetPasswordComponent},
        {path : 'customer-registration', component : CustomerRegistrationComponent },
        {path : 'choose-password',component : ChoosePasswordComponent},
        {path : 'service-request',component : ServiceRequestComponent,data: { breadcrumb: "Service Request"}},        
        {path : 'change-password',component : ChangePasswordComponent,data: {breadcrumb: "Change Password"} },
        {path : 'item-history',component : ItemHistoryComponent,data: {breadcrumb: "Item History"}},
        {path : 'favourites',component : FavouritesComponent,data: {breadcrumb: "Favourites"}},
        {path : 'my-account',component : MyAccountComponent,data: { breadcrumb: "My Account"}},
        {path : 'coming-soon',component : ComingSoonComponent,data: {breadcrumb: "Coming Soon"}},
        {path : 'search-customer',component : SearchCustomerComponent,data: {breadcrumb: "Search Customer"}},
        {path : 'user-maintenance',component : UserMaintenanceComponent,data: {breadcrumb: "User Maintenance"}},
        {path : 'item-detail',component : ItemDetailComponent,data: {breadcrumb: "Item Detail"}},
        {path : 'saved-orders',component : SavedOrdersComponent,data: {breadcrumb: "Saved Orders"}},    
        {path : 'item-search',component : ItemSearchComponent,data: {breadcrumb: "Item Search"}},
        {path : '',redirectTo : '/login',pathMatch : 'full'},
        {path : '**',component : LandingComponent,pathMatch : 'full'}
     ]

@NgModule({
    imports : [
        RouterModule.forRoot(routes, {useHash: true})
    ],
    exports : [RouterModule]
})
export class AppRouteModule{ }
export const routing: ModuleWithProviders = RouterModule.forRoot(routes);