import { States } from "./states";

export class BillingDetails {
    // Billing Details
    // MailAddress1 : string;
    // MailAddress2 : string;
    // MailCity : string;
    // MailState : string;
    // MailPostalCode : string;
    // MailCountry : string;
    // BillingToDoctorName: string;
    // PracticeName : string;
    // BillToContactName:string;
    // BillToPhone : string;
    // BillingEmailAddress:string;
    DoctorName: string;
    PracticeName: string;
    ContactName: string;
    PhoneNumber: string;
    EmailAddress: string;
    PrimaryAddress: string;
    SecondaryAddress: string;
    City: string;
    StateData: States[];
    ZipCode: string;
    Country: string;
    State: string;
}