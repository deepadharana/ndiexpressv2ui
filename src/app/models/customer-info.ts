import { BillingDetails } from './billing-details';
import { ShippingDetails } from './shipping-details';
import { CustomerTitles } from "./customer-title";

export class CustomerInfo {
    FirstName: string;
    LastName: string;
    TitleId: number;
    Title : CustomerTitles[];
    TitleValue : string;
    PhoneNumber: string;
    EmailAddress: string;
    FaxNumber: string;
    Password: string;
    ConfirmPassword: string;
    NDIAccountId: number;
    BillingAddress : BillingDetails;
    ShippingAddress :ShippingDetails;
}