export class SavedOrder{
    OrderId: number;
    OrderName: string;
    CreatorName: string;
    CustId: number;
    CustName: string;
    SavedOn: Date;
    OrderTotal: number;
    TotalItems: number;
  }