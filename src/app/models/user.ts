import { Account } from './account';

export class User{
    UserId: number;
    UserName: string;
    FirstName: string;
    MiddleName: string;
    LastName: string;
    Email: string;
    RowGuid: string;
    UserTypeId: number;
    UserType: string;
    RoleId: number;
    RoleName: string;
    AccessToken: string; 
    TokenExpireIn: number;
    Account: Account;
}