import { States } from "./states";

export class ShippingDetails {
    ShippingId: number;
    DoctorName: string;
    PracticeName: string;
    ContactName: string;
    PhoneNumber: string;
    EmailAddress: string;
    PrimaryAddress: string;
    SecondaryAddress: string;
    City: string;
    StateData: States[];
    ZipCode: string;
    Country: string;
    ShippingName: string;
    State: string;
}