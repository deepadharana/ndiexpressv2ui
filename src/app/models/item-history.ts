export class ItemHistory{
  Selected: boolean;
  Item: string;
  ItemId: number;
  Desc: string;
  ExtDesc: string;
  Brand: string;
  Order: string;
  Date: Date;
  Quantity: number;
  Availability: string;
  Price: string;
  IsVisible: boolean;
  Category: string;
  Promos: string[];
  IsExtDescVisible: boolean;
  YourPrice: string;
}