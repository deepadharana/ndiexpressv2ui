export interface CustomerInformation{
    CustomerId : number;
    CustomerName : string;
    Shippingid:number;
    City : string;
    Address1 : string;
    Address2  : string;
    PhoneNumber :string;
}