export class AcountRep{
    UserId: number;
    Name: string;
    DisplayName: string;
}

// export const AccountReps: AcountRep[] = [
//     {Id: 113456, Name: 'Sharon D.Purnel',DisplayName: ''},
//     {Id: 122456, Name: 'Larry T.Peterson',DisplayName: ''},
//     {Id: 124456, Name: 'Robert M.Patten',DisplayName: ''},
//     {Id: 123356, Name: 'David Dudzinski',DisplayName: ''},
//     {Id: 123556, Name: 'Anne E.Peak',DisplayName: ''}
// ]