export interface NewCustomerData{
    CreatedDate : string;
    Role : string;
    UserName : string;
    CustomerID : number;
    CustomerName  : string;
    PracticeEmailID :string;
}