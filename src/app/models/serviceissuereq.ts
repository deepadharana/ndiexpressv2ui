import { FileContent } from './filecontent';

export class ServiceIssueRequest{
    ItemName: string;
    Brand: string;
    IssueDetails: string;
    ItemLocation: string;
    AttachedImages: FileContent[];
    AttachedImageNames: string[];
}