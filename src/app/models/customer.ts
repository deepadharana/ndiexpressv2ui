import { AccountRep } from './AccountRep';
import { HomeOffice } from './HomeOffice';
import { Branch } from './Branch';
import { ShippingDetails } from './shipping-details';

export class Customer{
    CustomerId: number;
    CustomerName: string;
    ShipToLocation: ShippingDetails[];
    OfficePhone: string;
    OfficialEmail: string;
    SalesRepId: number;
    BranchMailId: string;
    AssignedSalesEmailId: string;
    Type: string;
    AccountRep: AccountRep;
    HomeOffice: HomeOffice;
    Branch: Branch;
    ContactPerson: string;
    FirstName : string;
    LastName : string;
    Title : string;
    PracticeName : string;
    EmailAddress : string;
    Password : string;
    ContactNumber : string;
    RePassword : string;
    NdiAccount : number;
    PracticeEmailId : string;
    BillToContactName  : string;
    ShipToContactName :string;
    FaxPhone:string;
    ShipToLocationValue :string;
}