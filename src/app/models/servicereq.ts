import { ServiceIssueRequest } from './serviceissuereq';

export class ServiceRequest {
    CustomerId: number;
    CustomerName: string;
    ShippingId: number;
    OfficialEmail: string;
    ContactPerson: string;
    BranchMailId: string;
    AssignedSalesEmailId: string;
    Type: string;
    Issues: ServiceIssueRequest[];
}