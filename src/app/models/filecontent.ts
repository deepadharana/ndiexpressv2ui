export class FileContent{
    AttachmentContentBytes: String;
    AttachmentType: string;
    AttachmentName: string;
    ImageSource: string;
    ImageUniqueId: number;
}