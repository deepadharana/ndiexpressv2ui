export class ShipToDetails{
    // Ship to details 
    PhysAddress1 : string;
    PhysAddress2 : string;
    PhysCity : string;
    PhysState : string;
    PhysPostalCode  : string;
    PhysCountry : string;
    ShipToDoctorName : string;
    ShipToPracticeName : string;
    ShipToContactName : string;
    ShipToPhone : string;
    EmailAddress : string;



}