import { NewItemHistory } from "../../assets/mock-data/newitem-detail";

export class ItemDetail {
    Item : NewItemHistory;
    Substitute : NewItemHistory[];
    Alternate : NewItemHistory[];
}