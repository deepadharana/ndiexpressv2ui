export class CustomerRequest{
    RequestID: number;
    RequestedDate: string;
    LastName: string;
    FirstName: string;
    PracticeName: string;
}