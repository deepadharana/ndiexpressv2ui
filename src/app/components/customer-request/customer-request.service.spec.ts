import { TestBed, inject } from '@angular/core/testing';

import { CustomerRequestService } from './customer-request.service';

describe('CustomerRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerRequestService]
    });
  });

  it('should be created', inject([CustomerRequestService], (service: CustomerRequestService) => {
    expect(service).toBeTruthy();
  }));
});
