import { CustomerRequest } from "../../models/customer-request";

export const UserMaintenanceCustomerData: CustomerRequest[] = [
    {
        RequestID: 123456,        
        RequestedDate: "2017-10-05",
        LastName: "Trevor C.",
        FirstName: "Taylor DDS",
        PracticeName: "Implant and Family Dntst"
    },
    {
        RequestID: 123456,        
        RequestedDate: "2017-10-05",
        LastName: "Trevor C.",
        FirstName: "Taylor DDS",
        PracticeName: "Implant and Family Dntst"
    },
    {
        RequestID: 123456,        
        RequestedDate: "2017-10-05",
        LastName: "Trevor C.",
        FirstName: "Taylor DDS",
        PracticeName: "Implant and Family Dntst"
    },
    {
        RequestID: 123456,        
        RequestedDate: "2017-10-05",
        LastName: "Trevor C.",
        FirstName: "Taylor DDS",
        PracticeName: "Implant and Family Dntst"
    }
]