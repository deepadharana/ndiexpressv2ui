import { Component, OnInit } from '@angular/core';
import { ApplicationService } from "../../common/application.service";
import { Router } from "@angular/router";
import { CustomerRequestService } from "./customer-request.service";

@Component({
  selector: 'app-customer-request',
  templateUrl: './customer-request.component.html',
  styleUrls: ['./customer-request.component.css'],
  providers: [CustomerRequestService]
})
export class CustomerRequestComponent implements OnInit {

  selectedID : boolean = false;  
  newCustAdd : boolean = false;
  accountIdEntered : boolean = false;
  items = [];

  pageIndex: number = 1;
  pageSize: number = 10;
  sortColumn: string = "test";
  sortOrder: boolean = true;
  searchId: number = 0;

  rowId: string;

  selectedItem = {};

  constructor(private _appService : ApplicationService,
              private router: Router,
              private _customerRequestService: CustomerRequestService) { }

  ngOnInit() {
    if(this._appService.currentUser==null){
      this.router.navigate(['/login']);
     }  

     this._customerRequestService.getRegistrationList(this.pageIndex, this.pageSize, this.sortColumn, this.sortOrder, this.searchId)
     .subscribe(response => {
       if (response.Success) {
         this.items = response.Content.Result;
         //console.log(this.items);
       }
     })   
    
  }

  selectedCustId(rowguid){
    this.selectedID = true;
    this.newCustAdd = false;
    //console.log(rowguid);

    this.rowId = rowguid;

    this._customerRequestService.getCustomerByID(this.rowId)
    .subscribe(response => {
      if (response.Success) {    
        this.selectedItem = response.Content.Result;
        //console.log(response.Content.Result);
      }
    }) 
  }

  addNewCustomer(rowguid){
    this.selectedID = false;
    this.newCustAdd = true;

    //console.log(rowguid);
    
    this.rowId = rowguid;

    this._customerRequestService.getCustomerByID(this.rowId)
    .subscribe(response => {
      if (response.Success) {    
        this.selectedItem = response.Content.Result;
        //console.log(response.Content.Result);
      }
    }) 
  }
  accountIdentered(){
    this.accountIdEntered= true;
  }

  cancel(){
    window.scrollTo(0,0);
    this.newCustAdd = false;
    this.selectedID = false;
  }
}
