import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { HttpService } from "../../common/http.service";
import { Response, Headers, URLSearchParams } from '@angular/http';

@Injectable()
export class CustomerRequestService {

  constructor(private _httpService : HttpService) { }

  public getRegistrationList(pageIndex: number, pageSize: number, sortColumn: string, sortOrder: boolean, searchId: number) : Observable <any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    return this._httpService.post('Registration/List', JSON.stringify({PageIndex: pageIndex, PageSize: pageSize, SortColumn: sortColumn, SortOrder: sortOrder, SearchId: searchId }), {headers: headers})
      .map(res=><any>res.json())
      .catch(error=> this.handleError(error))
  }

  public getCustomerByID(Id: string) : Observable <any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const params = new URLSearchParams();
    params.set('rowGuid', Id);
    
    return this._httpService.get('Registration/GetById', {headers: headers, search: params})
      .map(res=><any>res.json())
      .catch(error=> this.handleError(error))
  }

  handleError(error : any){
    if(error instanceof Response){
      return Observable.throw(error.json()['error'] || 'backend server error');
    }
  }

}
