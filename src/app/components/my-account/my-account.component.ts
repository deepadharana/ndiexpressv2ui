import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {

selectedNum: number = 1;
  active:boolean = true;
  constructor() { }

  ngOnInit() {
  }

  toggle(){
   this.active =! this.active;
  }

  displayData(number) {
this.selectedNum = number;
  }
}
