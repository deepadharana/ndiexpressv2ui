import { Component, OnInit } from '@angular/core';
import { RolesService } from "./roles.service";
import { RolesData } from "./roles-data";
import { Roles } from "./roles";

@Component({
  selector: 'app-roles-and-permission',
  templateUrl: './roles-and-permission.component.html',
  styleUrls: ['./roles-and-permission.component.css'],
  providers : [RolesService]
})
export class RolesAndPermissionComponent implements OnInit {

  roles : Roles[]=[];
  public open=false;
  status=true;
  constructor(private rolesService : RolesService) {
   // this.roles=RolesData;
   }

  ngOnInit() {
    this.getRolesList();    
  }

  getRolesList(){
    let currentuser = JSON.parse(localStorage.getItem('currentUser'));
    this.rolesService.getRoles(currentuser.UserTypeId).subscribe(response =>{
      this.roles=response.Content.Result;
      //console.log(this.roles);
    });
  }

  addNewRole(){
    this.open=!this.open;
  }

  checkExpandStatus(){
    this.status=!this.status;
  }
  

}
