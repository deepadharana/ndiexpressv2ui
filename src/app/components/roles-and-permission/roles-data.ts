import { Roles } from "./roles";

export const RolesData :  Roles[]=[
        {
            "RoleId" : 1,
            "RoleName" : "Admin"
        },
        {
            "RoleId" : 2,
            "RoleName" : "Sales Manger"
        },
        {
            "RoleId" : 3,
            "RoleName" : "Account Rep"
        },
        {
            "RoleId" : 4,
            "RoleName" : "CSR"
        },
        {
            "RoleId" : 5,
            "RoleName" : "Master Shopper"
        },
        {
            "RoleId" : 6,
            "RoleName" : "Normal Shopper"
        }
]