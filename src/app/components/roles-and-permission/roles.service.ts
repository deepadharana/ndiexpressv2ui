import { Injectable } from '@angular/core';
import { HttpService} from '../../common/http.service';
import { Observable } from 'rxjs/Rx';
import { Response, Headers, URLSearchParams } from '@angular/http';

@Injectable()
export class RolesService {

  constructor(private httpService :  HttpService) { }

  public getRoles(userTypeId) : Observable<any>{
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    const params = new URLSearchParams();
    params.set('userTypeId', userTypeId);

    return this.httpService.get('Role/List' , {headers : headers , search: params})
    .map(response=>response.json())
    .catch(error=>this.handleError(error));
  }

  handleError(error : any){
    if(error instanceof Response){
      return Observable.throw(error.json()['error'] || 'backend server error');
    }
  }
}
