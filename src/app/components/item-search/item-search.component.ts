import { Component, OnInit } from '@angular/core';
import { Persons } from '../item-history/data';
import { ItemHistory } from "../../models/item-history";
import { Availability } from '../item-history/data';
import { Brand } from '../item-history/data';

@Component({
  selector: 'app-item-search',
  templateUrl: './item-search.component.html',
  styleUrls: ['./item-search.component.css']
})
export class ItemSearchComponent implements OnInit {
  resultList = [];
  descSortDir: string = 'asc';
  extDescSortDir: string = 'asc';
  orderDateSortDir: string = 'asc';
  showFieldsOnMobile = false;
  allSelected: any;
  selectedAvailabilities = [];
  selectedBrands = [];
  availabilityList = [];
  brandList = [];

  constructor() {
    let searchText = localStorage.getItem('globalSearchText');
    if(searchText != undefined && searchText != ""){
      this.resultList =  Persons.filter((item: any) =>
      (item.ItemId.toString().indexOf(searchText) !== -1) ||
      item.Desc.toLocaleLowerCase().indexOf(searchText.toLocaleLowerCase()) !== -1 ||
      item.ExtDesc.toLocaleLowerCase().indexOf(searchText.toLocaleLowerCase()) !== -1 ||
      item.Brand.toLocaleLowerCase().indexOf(searchText.toLocaleLowerCase()) !== -1 ||
      //item.ProductGroup.toLocaleLowerCase().indexOf(searchText) !== -1 ||
      //item.Keywords.toLocaleLowerCase().indexOf(searchText) !== -1 ||
      //item.SubCategory.toLocaleLowerCase().indexOf(searchText) !== -1 ||
      item.Category.toLocaleLowerCase().indexOf(searchText.toLocaleLowerCase()) !== -1);
    }
    else{
      this.resultList =  Persons;
    }
    this.availabilityList = Availability;
    this.brandList = Brand;
   }
  ngOnInit() { }

  orderBy(column, direction) {
    if(direction == 'asc'){
      this.resultList = this.resultList.sort((a: any, b: any) => {
        if ( a[column] < b[column] ){
          return -1;
        }else if( a[column] > b[column] ){
            return 1;
        }else{
          return 0;	
        }
      });
      this.descSortDir = column == 'Desc' ? 'desc' : this.descSortDir;
      this.extDescSortDir = column == 'ExtDesc' ? 'desc' : this.extDescSortDir;
      this.orderDateSortDir = column == 'Date' ? 'desc' : this.orderDateSortDir;
    }
    else{
      this.resultList = this.resultList.sort((a: any, b: any) => {
        if ( a[column] > b[column] ){
          return -1;
        }else if( a[column] < b[column] ){
            return 1;
        }else{
          return 0;	
        }
      });
      this.descSortDir = column == 'Desc' ? 'asc' : this.descSortDir;
      this.extDescSortDir = column == 'ExtDesc' ? 'asc' : this.extDescSortDir;
      this.orderDateSortDir = column == 'Date' ? 'asc' : this.orderDateSortDir;
    }
  }

  showMoreFields() {
    this.showFieldsOnMobile = !this.showFieldsOnMobile;
  }

  selectAll() {
    let toggleStatus = !this.allSelected;
    this.resultList.forEach(function (eachitem, key) {
      eachitem.selected = toggleStatus;
    });
  }

  singleCheck() {
    this.allSelected = this.resultList.every(function(itm){ return itm.selected; });
  }

  updateSelectedAvailabilities(availability, action){
    if(action == 'select'){
      this.selectedAvailabilities.push(availability.text);
    }
    else{
      this.selectedAvailabilities = this.selectedAvailabilities.filter(x=>x!=availability.text);
    }
  }

  updateSelectedBrands(brand, action){
    if(action == 'select'){
      this.selectedBrands.push(brand.text);
    }
    else{
      this.selectedBrands = this.selectedBrands.filter(x=>x!=brand.text);
    }
  }
}
