import { Component, OnInit } from '@angular/core';
import { ItemDetailService } from "./item-detail.service";
import { ItemDetail } from "../../models/item-detail";
import { Persons, Persons2, PersonMain } from "../../../assets/mock-data/data";

declare var jQuery: any;

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css'],

  providers : [ItemDetailService]
})
export class ItemDetailComponent implements OnInit {

  status = false;
  altStatus=false;
  magnifyTheImage=false;
  detailStatus=false;
  imageSourceToMagnify='';

  item:any;
  substituteItems = [];
  leftSubstituteItems=[];
  rightSubstituteItems=[];
  alternateItems = [];

  columnsSubstitute;
  columnsAlternate;


  constructor(private itemDetailService : ItemDetailService) { 
     this.item=PersonMain[0];
     //console.log(this.item);
    
     this.substituteItems=Persons;

     this.alternateItems=Persons2;

     this.columnsSubstitute = Array.from(Array(Math.ceil(this.substituteItems.length / 2)).keys());
     this.columnsAlternate = Array.from(Array(Math.ceil(this.alternateItems.length / 2)).keys());
  }

  ngOnInit() {
  }

  increaseQuantity(itemId){
    if(this.item.ItemId == itemId){
      this.item.InitialQuantity=this.item.InitialQuantity+1;
    }
  }
  decreaseQuantity(itemId){
    if(this.item.ItemId==itemId){
      this.item.InitialQuantity=this.item.InitialQuantity-1;
    }
  }

  increaseSubstituteQuantity(itemId){
    this.substituteItems.forEach(x=> {
      if(x.ItemId == itemId)
        x.InitialQuantity = x.InitialQuantity + 1;
     })
  }
  decreaseSubstituteQuantity(itemId){
    this.substituteItems.forEach(x=> {
      if(x.ItemId == itemId && x.InitialQuantity > 0)
        x.InitialQuantity = x.InitialQuantity - 1;
     })
  }

  increaseAlternateProductsQuantity(itemId){
    this.alternateItems.forEach(x=> {
      if(x.ItemId == itemId)
        x.InitialQuantity = x.InitialQuantity + 1;
      })
  }
  decreaseAlternateProductsQuantity(itemId){
    this.alternateItems.forEach(x=> {
      if(x.ItemId == itemId && x.InitialQuantity > 0)
        x.InitialQuantity = x.InitialQuantity - 1;
     })
  }

  checkExpandStatus(){
    this.status=!this.status;
  }
  checkAltExpandStatus(){
    this.altStatus=!this.altStatus;
  }

  checkDetailExpandStatus(){
    this.detailStatus=!this.detailStatus;
  }

  magnifyImage(source) {
    this.magnifyTheImage = true;
    this.imageSourceToMagnify = source;
  }

}