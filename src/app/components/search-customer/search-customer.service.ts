import { Injectable } from '@angular/core';
import { Response, Headers, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { HttpService } from "../../common/http.service";

@Injectable()
export class SearchCustomerService {
 
  constructor(private _httpService: HttpService){ }

    getCustomers(accountRepId: number, searchText: string) : Observable<any> {
   
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');

      return this._httpService.get('Customer/Search?accountRepId=' + accountRepId + "&searchText=" + searchText, {headers: headers})
      .map(res => <any>res.json())
      .catch(this.handleError);
    }

    getCustomersData() : Observable<any> {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');

      return this._httpService.get('Customer/Search', {headers: headers})
      .map(res => <any>res.json())
      .catch(this.handleError);
    }

    getAccountReps() : Observable<any>{
     
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
  
      return this._httpService.get('User/AccountReps', {headers: headers})
      .map(res => <any>res.json())
      .catch(this.handleError);
    }
    
    private handleError(err : Response) {
      return Observable.throw(err.json().err || " Server error");
    }
}
