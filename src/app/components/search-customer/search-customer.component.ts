import { Component, OnInit } from '@angular/core';
import { AcountRep } from "../../models/acount-rep";
import { SearchCustomerService } from "./search-customer.service";
import { ApplicationService } from "../../common/application.service";
import { Router } from "@angular/router";
import { AppComponent } from "../app.component";
import { CustomerInformation } from "../../models/customer-information";

@Component({
  selector: 'app-search-customer',
  templateUrl: './search-customer.component.html',
  styleUrls: ['./search-customer.component.css'],
  providers: [SearchCustomerService]
})
export class SearchCustomerComponent implements OnInit {
 // selectedValue: any;
  active:Array<string>;
  repList = [];
  //selectedRep: AcountRep = new AcountRep();
  displayDiv: boolean = false;
  accountReps: AcountRep[]; //= [ new AcountRep() ];
  customer: CustomerInformation[] = [];
  _listFilter: string;
  filteredCustomer: CustomerInformation[];
  errorMessage: string;
  selectedRow: Number;
  showBorder:boolean = false;

  get listFilter(): string {
    return this._listFilter;
  }

  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredCustomer = this.listFilter ? this.performFilter(this.listFilter) : this.customer;
  }

  constructor(private searchCustomerService: SearchCustomerService,
    private _appService: ApplicationService,
    private router: Router, private _app: AppComponent) { }

  ngOnInit() {
    if (this._appService.currentUser == null) {
      this.router.navigate(['/login']);
    }
    let currentuser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentuser!=null){
      if(currentuser.UserType === "Customer" || currentuser.RoleName === "CSR"){
        this.displayDiv=true;
        this.searchCustomerService.getCustomersData()
        .subscribe(data => {
          this.customer = data.Content.Result;
          this.filteredCustomer = this.customer;
        },
        error => this.errorMessage = <any>error
        );        
       }else if(currentuser.RoleName === "AccountRep"){
        this.displayDiv=true;
        this.searchCustomerService.getCustomers(currentuser.UserId, "")
        .subscribe(data => {
          this.customer = data.Content.Result;
          this.filteredCustomer = this.customer;
        },
        error => this.errorMessage = <any>error
        );
       }
       else{
        this.searchCustomerService.getAccountReps().subscribe(data => {
          this.accountReps = data.Content.Result;
    
          if(this.accountReps.length != undefined){
            for (var i = 0; i < this.accountReps.length; i++) {
              this.accountReps[i].DisplayName = this.accountReps[i].UserId + " " + this.accountReps[i].Name;
              this.repList.push(this.accountReps[i].DisplayName);
            }
            // this.selectedRep.UserId = this.accountReps[0].UserId;
            // this.selectedRep.Name = this.accountReps[0].Name;
            // this.selectedRep.DisplayName = this.accountReps[0].DisplayName;
            // this.selectedValue = this.accountReps[0].DisplayName;
          } 
        })
       }
     }
  }
  public selected(value: any): void {
    this.showBorder = true;
    let userId=0;
    userId = value.id.split(" ")[0];
    this.displayDiv = true;
    this.searchCustomerService.getCustomers(userId, "")
      .subscribe(data => {
        this.customer = data.Content.Result;
        this.filteredCustomer = this.customer;
      },
      error => this.errorMessage = <any>error
      );
    }

  setClickedRow(index) {
    this.selectedRow = index;
    let selectedCust = this.filteredCustomer[index];
    localStorage.setItem('selected-customer-detail', JSON.stringify(selectedCust));
    this._app.isCustSelected = true;
    if(selectedCust.Address2 == ""){
      this._app.custDetail = selectedCust.CustomerId + ' | ' +
      selectedCust.CustomerName + ' | ' + selectedCust.Address1;
    }else{
      this._app.custDetail = selectedCust.CustomerId + ' | ' +
      selectedCust.CustomerName + ' | ' + selectedCust.Address1 + ',' + selectedCust.Address2;
    }
    
    setTimeout(() => {
      let path = localStorage.getItem('navigationPath');
      if (path != null) {
        this.router.navigate([path]);
      }
      else if (window.history != null) {
        window.history.back();
      }
      else {
        this.router.navigate(['/home']);
      }
    }, 1000);
  }

  performFilter(filterBy: string): CustomerInformation[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.customer.filter((product: CustomerInformation) =>
      (product.CustomerName.toLocaleLowerCase().indexOf(filterBy) !== -1) ||
      product.CustomerId.toString().indexOf(filterBy) !== -1 ||
      product.City.toLocaleLowerCase().indexOf(filterBy) !== -1 ||
      product.Address1.toLocaleLowerCase().indexOf(filterBy) !== -1 ||
      product.Address2.toLocaleLowerCase().indexOf(filterBy) !== -1 ||
      product.PhoneNumber.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }
}