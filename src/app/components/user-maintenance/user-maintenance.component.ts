import { Component, OnInit } from '@angular/core';
import { ApplicationService } from "../../common/application.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-user-maintenance',
  templateUrl: './user-maintenance.component.html',
  styleUrls: ['./user-maintenance.component.css']
})
export class UserMaintenanceComponent implements OnInit {

  selectedNum: number = 1;

  constructor(private _appService : ApplicationService,private router : Router) { }

  ngOnInit() {
    if(this._appService.currentUser==null){
      this.router.navigate(['/login']);
     }
  }
 
  displayData(number) {
    this.selectedNum = number;
  }

}
