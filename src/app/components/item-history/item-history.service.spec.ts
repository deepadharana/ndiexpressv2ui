import { TestBed, inject } from '@angular/core/testing';

import { ItemHistoryService } from './item-history.service';

describe('ItemHistoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ItemHistoryService]
    });
  });

  it('should be created', inject([ItemHistoryService], (service: ItemHistoryService) => {
    expect(service).toBeTruthy();
  }));
});
