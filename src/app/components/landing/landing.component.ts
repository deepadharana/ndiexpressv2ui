import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApplicationService } from "../../common/application.service";
import { User } from "../../models/user";
import { AppComponent } from "../app.component";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  currentUser : User;
  constructor(private _router: Router,
              private _appService: ApplicationService,
              private appComponent : AppComponent ) { }
 
  ngOnInit() {
    if(this._appService.currentUser==null){
      this.appComponent.isLoggedIn = false;
      this._router.navigate(['/login']);
    }
  }
}
