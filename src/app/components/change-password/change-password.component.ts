import { Component, OnInit, ViewChild } from '@angular/core';
import { ChangePasswordService } from "./change-password.service";
import { TemplateHeaderComponent } from "../../common/template/template-header/template-header.component";
import { ApplicationService } from "../../common/application.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css'],
  providers: [ChangePasswordService]
})
export class ChangePasswordComponent implements OnInit {

  @ViewChild('f') form:any;
  capsOn = false;
  wrongPattern = false;
  successMessage = '';
  failureMessage = '';
  alertMsg = false;
  oldPasswordField="";
  passwordField="";
  confirmPasswordField="";

  constructor(private _changePasswordService: ChangePasswordService,private router : Router,
              private _appService: ApplicationService) { }

  ngOnInit() {
    if(this._appService.currentUser==null){
     this.router.navigate(['/login']);
    }
  }

  capsLock(e) {
    let kCode = e.keyCode ? e.keyCode : e.which;
    let sKey = e.shiftKey ? e.shiftKey : ((kCode == 16) ? true : false);
    if (((kCode >= 65 && kCode <= 90) && !sKey) || ((kCode >= 97 && kCode <= 122) && sKey)) {
      this.capsOn = true;
    }
    else {
      this.capsOn = false;
    }
  }

  testPassword(password) {
    let Password_REGEX = /^(?=(.*?[A-Z]){2,})(?=(.*[a-z]){2,})(?=(.*[\d]){2,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/;
    if (!Password_REGEX.test(password)) {
      this.wrongPattern = true;
    }
    else {
      this.wrongPattern = false;
    }
  }

  updatePassword(oldPassword, password, confirmPassword) {
    this.alertMsg = true;
    this._changePasswordService.setChangePassword(oldPassword, password, confirmPassword)
      .subscribe(response => {
        let data = response.Content.Errors;

        if (response.Success) {
          this.successMessage = response.Message;
          setTimeout(() => {
            this._appService.notifyChild({ data: "true" });
          }, 3000);


        } else if (data.CurrentPassword != undefined) {
          this.failureMessage = data.CurrentPassword;
        } else if (data.Password != undefined) {
          this.failureMessage = data.Password;
        } else if (data.ConfirmPassword != undefined) {
          this.failureMessage = data.ConfirmPassword;
        } else {
          this.failureMessage = response.Message;
        }
      })
  }
  close() {
    this.alertMsg = false;
  }

  cancel(){
    this.form.reset();
}

}
