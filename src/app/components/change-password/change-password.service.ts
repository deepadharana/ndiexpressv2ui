import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { HttpService } from "../../common/http.service";
import { Response, Headers, URLSearchParams } from '@angular/http';

@Injectable()
export class ChangePasswordService {
  constructor(private _httpService: HttpService) { }

  public setChangePassword(CurrentPassword: string, Password : string, ConfirmPassword :string) : Observable <any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._httpService.post('ChangePassword', JSON.stringify({CurrentPassword, Password, ConfirmPassword}), {headers: headers})
        .map(res=><any>res.json())
        .catch(error=> this.handleError(error));
  }

  handleError(error : any){
    if(error instanceof Response){
      return Observable.throw(error.json()['error'] || 'backend server error');
    }
  }
}
