import { Component, OnInit } from '@angular/core';
import { LoginService } from "./login.service";
import { Router } from '@angular/router';
import { User } from '../../models/user';
import { ApplicationService } from '../../common/application.service';
import { AppComponent } from "../app.component";
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})

export class LoginComponent implements OnInit {
  userName = "";
  password = "";
  userBlankMsg = "";
  passwordBlankMsg = "";
  allFieldsBlankMsg = "";
  wrongDetailsMsgs = "";
  userErrorMsg = false;
  passwordErrorMsg = false;
  wrongDetailsMsg = true;
  capsMsg = true;
  statusUserName = true;
  clickCount : number = 0;
  accountLockMsg = "";
  userData: User = new User();

  constructor(private _loginService: LoginService,
              private _appService: ApplicationService,
              private _router: Router,
              private idle: Idle,
              private keepalive: Keepalive,
              private appComponent : AppComponent) { }

  ngOnInit() { 
    if(this._appService.currentUser==null){
      this.appComponent.isLoggedIn = false;
      this._router.navigate(['/login']);
    }
    else{
      this._router.navigate(['/home']);
    }
   
    this.idle.setIdle(1);
    this.idle.setTimeout(1);
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.onTimeout.subscribe(() => {
     //console.log('Timed out!');console.log(localStorage.getItem('currentUser'));
      this._appService.logoutUser().subscribe(data => {
        if (data.Success || data.Content.Errors.AuthToken != undefined) {
          localStorage.setItem('currentUser', null);      
          this._appService.currentUser = null;
          this._appService.setUserLoggedIn(false);
          this.appComponent.isUserLoggedIn();   
          this.appComponent.isCustSelected = false;   
          this._router.navigate(["/login"]);
          localStorage.isUserTimedout = "true";
        }
      });
    });
  }

  login(userName, password) {
    this.clickCount=this.clickCount+1;
      this._loginService.login(userName, password)
      .subscribe(response => {
        if(response.Content.Errors.AccountLocked==undefined){
          if (response.Success) {
            this.idle.setTimeout(response.Content.Result.TokenExpireIn * 60);
            this.idle.watch();
            this.userData = response.Content.Result;
            //console.log(this.userData);
            if(localStorage.isUserTimedout == "true"){
        
              let user = JSON.parse(localStorage.getItem('currentUser'));
              if(user == null){
                localStorage.setItem('currentUser', JSON.stringify(this.userData));      
                this._appService.currentUser = this.userData;
                localStorage.isUserTimedout = "false" ;
                this._appService.setUserLoggedIn(true);
              }
              else{
                this._appService.currentUser = user;
                this._appService.selectedCust = JSON.parse(localStorage.getItem('selected-customer-detail'));
                localStorage.isUserTimedout = "false";
                this._appService.setUserLoggedIn(true);
                if(this._appService.selectedCust != null || this._appService.selectedCust != undefined){
                  this.appComponent.custDetail = this._appService.selectedCust.CustomerId + ' | ' +
                                                 this._appService.selectedCust.CustomerName + ' | ' +
                                                 this._appService.selectedCust.City;
                  this.appComponent.isCustSelected = true;   
                  }
                this.appComponent.isUserLoggedIn();
              }
            }
            else{
              localStorage.setItem('currentUser', JSON.stringify(this.userData));      
              localStorage.setItem('selected-customer-detail', null);
              this.appComponent.custDetail = '';
              this.appComponent.isLoggedIn = true;
              this._appService.selectedCust = null;
              this._appService.currentUser = this.userData;
              this._appService.setUserLoggedIn(true);
            }
            this.appComponent.isUserLoggedIn();
            this.appComponent.isLoggedIn = true;
            this._router.navigate(['/home']);
          }
          else {
            this._appService.setUserLoggedIn(false);
            let data = response;
            this.wrongDetailsMsg = false;
            this.userData = response.Content.Errors;
            if (userName == "" && password == "") {
              this.userErrorMsg = true;
              this.passwordErrorMsg = true;
              this.userBlankMsg= response.Content.Errors.UserName;
              this.passwordBlankMsg=response.Content.Errors.Password;
            } else if (password == "") {
              this.passwordErrorMsg = true;
              this.userErrorMsg = false;
              this.passwordBlankMsg=response.Content.Errors.Password;
            } else if (userName == "") {
              this.userErrorMsg = true;
              this.passwordErrorMsg = false;
              this.userBlankMsg= response.Content.Errors.UserName;
            }
            else {
              this.userErrorMsg = false;
              this.passwordErrorMsg = false;
              if(response.Content.Errors.InValidUsername != undefined){
              this.wrongDetailsMsgs=response.Content.Errors.InValidUsername;
            }
            if(response.Content.Errors.InValidUsernameOrPassword != undefined){
              this.wrongDetailsMsgs=response.Content.Errors.InValidUsernameOrPassword;
            }
          }
        }
      }
      else{
        this.accountLockMsg=response.Content.Errors.AccountLocked;
      }
    });
  }

  register(){
    this._router.navigate(['./customer-registration']);
  }

  capsLock(e) {
    let kCode = e.keyCode ? e.keyCode : e.which;
    let sKey = e.shiftKey ? e.shiftKey : ((kCode == 16) ? true : false);
    if (((kCode >= 65 && kCode <= 90) && !sKey) || ((kCode >= 97 && kCode <= 122) && sKey)) {
      this.capsMsg = false;
    }
    else {
      this.capsMsg = true;
    }
    this.accountLockMsg = "";
  }

  refreshErrors(){
    this.userErrorMsg = false;
    this.passwordErrorMsg = false;
    this.wrongDetailsMsg = true;
    this.capsMsg = true;
  }

  emailCheck(userName){
    let EMAIL_REGEXP = /([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/; 
    if(userName!=null && (userName.length<=5 || !  EMAIL_REGEXP.test(userName))){
      this.statusUserName=false;
    }
    else{
      this.statusUserName=true;
    }  
  }
}