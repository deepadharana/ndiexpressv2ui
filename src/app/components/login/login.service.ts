import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { User } from '../../models/user';
import { HttpService } from "../../common/http.service";
import { Response, Headers, URLSearchParams } from '@angular/http';

@Injectable()
export class LoginService {
  constructor(private _httpService : HttpService) { }

  public login(userName, password): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._httpService.post('Account/Login', JSON.stringify({userName : userName , password : password}), {headers: headers})
      .map(res=><any>res.json())
      .catch(this.handleError);
  }

  handleError(error : any){
    if(error instanceof Response){
      return Observable.throw(error.json()['error'] || 'backend server error');
    }
  }
}
