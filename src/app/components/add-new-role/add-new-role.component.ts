import { Component, OnInit } from '@angular/core';
import { Features } from "./features";
import { NewPermission } from "./permission-data";
import { Router } from "@angular/router";
import { RolesAndPermissionComponent } from "../roles-and-permission/roles-and-permission.component";

@Component({
  selector: 'app-add-new-role',
  templateUrl: './add-new-role.component.html',
  styleUrls: ['./add-new-role.component.css']
})
export class AddNewRoleComponent implements OnInit {

  permissions=[];
  distinctPermission=[];
  feature :  Features[] = [];
  status=true;
  constructor(private rolePermissionComponent : RolesAndPermissionComponent) {
    this.permissions=NewPermission;
    //console.log(this.permissions);
   }

  ngOnInit() {
  }
  navigateToRoles(){
    this.rolePermissionComponent.open = false;
  }
  checkExpandStatus(){
    this.status=!this.status;
  }

}
