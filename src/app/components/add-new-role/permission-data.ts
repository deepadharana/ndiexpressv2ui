import { Permission } from "./permission";
import { Features } from "./features";

export const NewPermission: Features[] = [

    {
        FeatureId : 1,
        FeatureName : 'User Maintainance',
        Permission : [
            {
                PermissionId : 1,
                PermissionName : 'Add Customer'
            },
            {
                PermissionId : 2,
                PermissionName : 'Add NDI User'
            },
            {
                PermissionId : 3,
                PermissionName : 'Activate Accounts'
            },
            {
                PermissionId : 4,
                PermissionName : 'Inactivate Accounts'
            },
            {
                PermissionId : 5,
                PermissionName : 'Assign roles to users'
            },
            {
                PermissionId : 6,
                PermissionName : 'Edit Permission of a role'
            },
            {
                PermissionId : 7,
                PermissionName : 'Create new role'
            },
            {
                PermissionId : 8,
                PermissionName : 'Edit Customer'
            },
            {
                PermissionId : 9,
                PermissionName : 'Edit NDI User'
            },
            {
                PermissionId : 10,
                PermissionName : 'Lock Accounts'
            }
        ]
    },
    {
        FeatureId : 2,
        FeatureName : 'Item History',
        Permission : [
            {
                PermissionId : 1,
                PermissionName : 'On hand quantity'
            },
        ]
    },
    {
        FeatureId : 3,
        FeatureName : 'Saved Orders',
        Permission : [
            {
                PermissionId : 1,
                PermissionName : 'Hide Favourite Items'
            },
            {
                PermissionId : 2,
                PermissionName : 'Unhide Favourite Items'
            },
            {
                PermissionId : 3,
                PermissionName : 'Bulk Order placement'
            },
        ]
    },
    {
        FeatureId : 4,
        FeatureName : 'Cart Page',
        Permission : [
            {
                PermissionId : 1,
                PermissionName : 'Requested by'
            },
            {
                PermissionId : 2,
                PermissionName : 'Requested to'
            },
            {
                PermissionId : 3,
                PermissionName : 'Hide for verification'
            },
            {
                PermissionId : 4,
                PermissionName : 'Place order'
            },
            {
                PermissionId : 5,
                PermissionName : 'Benevolent Order'
            },
            {
                PermissionId : 6,
                PermissionName : 'View gross profit'
            },
        ]
    },

]