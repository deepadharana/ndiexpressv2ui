import { Permission } from "./permission";

export class Features {
    FeatureId : number;
    FeatureName : string;
    Permission : Permission[]; 
}