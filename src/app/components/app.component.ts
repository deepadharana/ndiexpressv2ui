import { Component, OnInit } from '@angular/core';
import { HashLocationStrategy, Location, LocationStrategy } from '@angular/common';
import { ApplicationService } from "../common/application.service";
import { HttpService } from "../common/http.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ApplicationService, HttpService, ApplicationService, { provide: LocationStrategy, useClass: HashLocationStrategy }],
})

export class AppComponent implements OnInit {
  public loading = false;
  isLoggedIn: boolean;
  selectedCust: any;
  isCustSelected : boolean=false;
  custDetail: string;
  location: Location;

  constructor(private _appService: ApplicationService,
              private _httpService: HttpService,
              private _Application: ApplicationService,
              location: Location) { 
                this.location = location; 
               }

  ngOnInit() {
    this.isUserLoggedIn();
    if (this._appService.selectedCust != null) {
      this.selectedCust = this._appService.selectedCust;
      this.isCustSelected=true;

      if(this.selectedCust.Address2 == "") {
        this.custDetail = this.selectedCust.CustomerId + ' | ' +
        this.selectedCust.CustomerName + ' | ' +
         this.selectedCust.Address1;
      }
      else {
        this.custDetail = this.selectedCust.CustomerId + ' | ' +
        this.selectedCust.CustomerName + ' | ' +
         this.selectedCust.Address1+','+this.selectedCust.Address2;
      }
      
    }
  }

  isUserLoggedIn(): void {
    this.isLoggedIn = this._appService.getUserLoggedIn();
    if (this._appService.currentUser) {
      this.isLoggedIn = true;
    }
    else {
      this.isLoggedIn = false;
    }
  }
}