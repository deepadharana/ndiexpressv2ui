import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ShipToDetails } from '../../models/ship-to-details';
import { BillingDetails } from '../../models/billing-details';
import { CustomerInfo } from '../../models/customer-info';
import { HttpService } from "../../common/http.service";
import { Response, Headers, URLSearchParams } from '@angular/http';

@Injectable()
export class CustomerRegistrationService{
    constructor(private _httpServive : HttpService){}
    
    public createRegistrationRequest(customer : CustomerInfo ) : Observable<any>{
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this._httpServive.post('Customer/Registration', JSON.stringify(customer), {headers: headers})
        .map(res=><any>res.json())
        .catch(error=>this.handleError(error));
    }
    
    public getTitles() : Observable<any>{
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this._httpServive.get('GetTitles', {headers: headers})
        .map(res=><any>res.json())
        .catch(error=>this.handleError(error));
    }
    public getStates() : Observable<any>{
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this._httpServive.get('GetStates', {headers: headers})
        .map(res=><any>res.json())
        .catch(error=>this.handleError(error));
    }
    
    handleError(error : any ){
        if(error instanceof Response){
            return Observable.throw(error.json()['error'] || 'backend server error');
        }
    }
}