import { Component, OnInit, ViewChild } from '@angular/core';
import { BillingDetails } from '../../models/billing-details';
import { ShipToDetails } from '../../models/ship-to-details';
import { CustomerRegistrationService } from './customer-registration.service';
import { CustomerTitles } from '../../models/customer-title';
import { ApplicationService } from "../../common/application.service";
import { CustomerInfo } from '../../models/customer-info';
import { ShippingDetails } from "../../models/shipping-details";
import { States } from "../../models/states";
import { Router } from '@angular/router';
import { AppComponent } from "../app.component";

@Component({
  selector: 'app-customer-registration',
  templateUrl: './customer-registration.component.html',
  styleUrls: ['./customer-registration.component.css'],
  providers : [CustomerRegistrationService]
})
export class CustomerRegistrationComponent implements OnInit {
  @ViewChild('f') form : any;
  model: CustomerInfo = new CustomerInfo();
  modelBilling: BillingDetails = new BillingDetails();
  modelShipDetails: ShippingDetails = new ShippingDetails();
  selectedTitle : CustomerTitles = new CustomerTitles();  
  checkBoxStatus : boolean;
  passwordRules : boolean;
  alertMsg = false;
  successMessage=false;
  failureMessage=false;
  headerInfo=false;
  submissionAlert = false;
  errorMsg='';
  wrongPattern=false;
  submitStatus=false;
  errorRef=false;

  Title = [ new CustomerTitles() ];
  StateData = [ new States()];

  constructor(private regService : CustomerRegistrationService,
    private _appService: ApplicationService,
    private _router: Router,
    private _app :AppComponent) {}

  ngOnInit() {
    if(this._appService.currentUser != null){
      this._router.navigate(['/home']);
    }
    this.submitStatus=false;
    this.alertMsg=false;
    this.submissionAlert = false;
    this.getTitles();
  }
  register(value){
    this._app.loading=true;
    this.model.BillingAddress = this.modelBilling;
    this.model.ShippingAddress = this.modelShipDetails;
    if(this.model.EmailAddress == undefined){this.model.EmailAddress = '';}
    if(this.model.FaxNumber == undefined){this.model.FaxNumber = '0';}
    if(this.model.NDIAccountId == undefined){this.model.NDIAccountId = 0;}
    this.regService.createRegistrationRequest(this.model)
    .subscribe(response =>{
      if(response.Success){
        this.submitStatus=true;
        this._app.loading=false;
        this.alertMsg=true;
        this.successMessage=true;
        this.submissionAlert = true;
        this.form.reset();        
      }else{
        this._app.loading=false;
        this.alertMsg=false;
        this.failureMessage=false;
        this.submissionAlert = false;
      }
    },
    error=>{ });
  }

  getTitles(){
    this.regService.getTitles()
    .subscribe(response=>{
      if(response.Success){
        this.Title = response.Content.Result;
        this.model.TitleId = response.Content.Result[0].TitleId;
      }
      else{ }
    },
  error=>{ });
  }
  getStates(){
    this.regService.getStates()
    .subscribe(response=>{
      if(response.Success){
        this.StateData=response.Content.Result;
        this.modelBilling.State = response.Content.Result[0].StateName;
        this.modelShipDetails.State = response.Content.Result[0].StateName;
      }
    })
  }
  updateState(value){
      this.modelBilling.State=value;
  }
  processChange(e){
    if(e){
      this.checkBoxStatus=true;
      this.modelShipDetails.DoctorName=this.modelBilling.DoctorName;
      this.modelShipDetails.PracticeName=this.modelBilling.PracticeName;
      this.modelShipDetails.ContactName=this.modelBilling.ContactName;
      this.modelShipDetails.EmailAddress=this.modelBilling.EmailAddress;
      this.modelShipDetails.PhoneNumber=this.modelBilling.PhoneNumber;
      this.modelShipDetails.PrimaryAddress = this.modelBilling.PrimaryAddress;
      this.modelShipDetails.SecondaryAddress = this.modelBilling.SecondaryAddress;
      this.modelShipDetails.City=this.modelBilling.City;
      this.modelShipDetails.StateData=this.modelBilling.StateData;
      this.modelShipDetails.State=this.modelBilling.State;
      this.modelShipDetails.Country=this.modelBilling.Country;
      this.modelShipDetails.ZipCode=this.modelBilling.ZipCode;
    }else{
      this.checkBoxStatus=false;
      this.revertChanges();
    }
  }

  resetFields(value){
    this.form.reset();   
  }
 
  revertChanges(){
    this.modelShipDetails.PrimaryAddress='';
    this.modelShipDetails.SecondaryAddress='';
    this.modelShipDetails.City='';
    this.modelShipDetails.Country='';
    this.modelShipDetails.ZipCode='';
    this.modelShipDetails.DoctorName='';
    this.modelShipDetails.EmailAddress='';
    this.modelShipDetails.PhoneNumber='';
    this.modelShipDetails.ContactName='';
    this.modelShipDetails.PracticeName='';
  }

  openInfo(){
    this.headerInfo=!this.headerInfo;
  }

  validateFirstName(){
    let _firstName=this.model.FirstName;
    this.validateFName(_firstName);
  }
  validateLastName(){
    let _lastName=this.model.LastName;
    this.validateLName(_lastName);
  }
  validateEmailAddress(){
    let _customerEmail=this.model.EmailAddress;
    this.validateEmail(_customerEmail);
  }
  validateContactNumber(){
    let _contactNumber=this.model.PhoneNumber;
    this.validateNumber(_contactNumber);
  }
  validateFaxNumber(){
    let _faxNumber=this.model.FaxNumber;
    this.validateFax(_faxNumber);
  }
  validateCustomerPassword(){
    let _customerPassword=this.model.Password;
    this.validatePassword(_customerPassword);
  }
  confirmPassword(){
    let _password = this.model.Password;
    let _reEnteredPassword=this.model.ConfirmPassword;
    this.validateConfirmPassword(_password,_reEnteredPassword);
  }
  validateNdiAccount(){
    let _ndiAccount=this.model.NDIAccountId;
    this.validateNdi(_ndiAccount);
  }

  validateBillToDoctorName(){
    let _billToDoctorName=this.modelBilling.DoctorName;
    this.validateDoctorName(_billToDoctorName);
  }
  validateBillToPracticeName(){
    let _billToPracticeName=this.modelBilling.PracticeName;
    this.validateName(_billToPracticeName);
  }
  validateBillToContactName(){
    let _billToContactName=this.modelBilling.ContactName;
    this.validateName(_billToContactName);
  }
  validateBillToEmail(){
    let _billToEmail=this.modelBilling.EmailAddress;
    this.validateEmail(_billToEmail);
  }
  validateBillToPhone(){
    let _billToPhone=this.modelBilling.PhoneNumber;
    this.validateNumber(_billToPhone);
  }
  validateBillingCountry(){
    let _billingCountry=this.modelBilling.Country;
    this.validateCountry(_billingCountry);
  }
  validateBillingCity(){
    let _billingCity=this.modelBilling.City;
    this.validateCity(_billingCity);
  }
  validateBillingState(){
    let _billingState=this.modelBilling.State;
    this.validateState(_billingState);
  }
  validateBillingZip(){
    let _billingZip=this.modelBilling.ZipCode;
    this.validateZip(_billingZip);
  }

  validateShipToDoctorName(){
    let _shipToDoctorName=this.modelShipDetails.DoctorName;
    this.validateDoctorName(_shipToDoctorName);
  }
  validateShipToPracticeName(){
    let _shipToPracticeName=this.modelShipDetails.PracticeName;
    this.validateName(_shipToPracticeName);
  }
  validateShipToContactName(){
    let _shipToContactName=this.modelShipDetails.ContactName;
    this.validateName(_shipToContactName);
  }
  validateShipToEmail(){
    let _shipToEmail=this.modelShipDetails.EmailAddress;
    this.validateEmail(_shipToEmail);
  }
  validateShipToPhone(){
    let _shipToPhone=this.modelShipDetails.PhoneNumber;
    this.validateNumber(_shipToPhone);
  }
  validateShippingCountry(){
    let _shippingCountry=this.modelShipDetails.Country;
    this.validateCountry(_shippingCountry);
  }
  validateShippingCity(){
    let _shippingCity=this.modelShipDetails.City;
    this.validateCity(_shippingCity);
  }
  validateShippingState(){
    let _shippingState=this.modelShipDetails.State;
    this.validateState(_shippingState);
  }
  validateShippingZip(){
    let _shippingZip=this.modelShipDetails.ZipCode;
    this.validateZip(_shippingZip);
  }

  validatePassword(password){
    this.errorMsg='';
    let passwordPattern = /^(?=(.*?[A-Z]){2,})(?=(.*[a-z]){2,})(?=(.*[\d]){2,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/;
    if(password!=null && password!=''){
      let am = passwordPattern.test(this.model.Password);
      if(passwordPattern.test(this.model.Password)){
        this.wrongPattern=false; 
        return true;
      }else{
        this.errorMsg='Invalid password';
        this.wrongPattern=true;
        return false;
      }
    }
  }

  validateEmail(email){
    this.errorMsg='';
    let emailPattern = /([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    if(email!=null && email!=''){
      if(emailPattern.test(email)){
        return true;
      }else{
        this.errorMsg='Invalid email';
        return false;
      }
    }
  }
  validateFName(name){
    this.errorMsg='';
    let namePattern = /^[a-zA-Z_]*$/
    if(name!=null && name!=''){
      if(namePattern.test(name.trim())){
        return true;
      }else{
        this.errorMsg='Invalid name';
        return false;
      }
    }
  }
  validateLName(name){
    this.errorMsg='';
    let namePattern = /^[a-zA-Z_]*$/   
    if(name!=null && name!=''){
      let am = namePattern.test(name.trim());
      if(namePattern.test(name.trim())){
        return true;
      }else{
        this.errorMsg='Invalid name';
        return false;
      }
    }
  }
  validateDoctorName(name){
    this.errorMsg='';
    let namePattern = /^[a-zA-Z,]*\.?[\sa-zA-z,]*$/
    if(name!=null && name!=''){
      if(namePattern.test(name.trim())){
        return true;
      }else{
        this.errorMsg='Invalid name';
        return false;
      }
    }
  }
  validateName(name){
    this.errorMsg='';
    let namePattern = /^[\sa-zA-Z\s]*$/
    if(name!=null && name!=''){
      let am = namePattern.test(name);
      if(namePattern.test(name)){
        return true;
      }else{
        this.errorMsg='Invalid name';
        return false;
      }
    }
  }
  validateNumber(contactNumber){
    this.errorMsg='';    
    let contactNumberPattern=/^[0-9]*$/
    if(contactNumber!=null && contactNumber!=''){
      if(contactNumber>10){
        if(contactNumberPattern.test(contactNumber)){ 
          return true;
        }else{
          this.errorMsg='Incorrect phone number';
          return false;  
        }
      }else{
        this.errorMsg='Incorrect phone number';
        return false;  
      }
      
    }
  }
  validateFax(faxNumber){
    this.errorMsg='';    
    let faxNumberPattern=/^[0-9]{10}$/
    if(faxNumber!=null && faxNumber!=''){
      let am=faxNumberPattern.test(faxNumber);
      if(faxNumber>10){
        if(faxNumberPattern.test(faxNumber)){
          return true;
        }else{
          this.errorMsg='Invalid fax number';
          return false;  
        }
      }else{
        this.errorMsg='Invalid fax number';
        return false; 
      }
      
    }
  }  
  validateConfirmPassword(_password,reEnteredPassword){
    this.errorMsg='';    
    if(_password!=null && reEnteredPassword!=null){
      if(_password.trim()==reEnteredPassword.trim()){
        return true;
      }else if(reEnteredPassword != ''){
        this.errorMsg='Confirm Password does not match';
        return false;
      }else{
        //this.errorMsg='Required';
        return false;
      }
    }else{
    }
  }
  validateNdi(ndiAccount){
    this.errorMsg='';    
    if(ndiAccount!=null && ndiAccount!=''){
      let ndiPattern=/^[0-9]{10}$/;
      if(ndiAccount>10){
        if(ndiPattern.test(ndiAccount)){
          return true;
        }else{
          return false;  
        }
        }else{
          this.errorMsg='Invalid Ndi number';
          return false;
        }
      }
    }

  validateCountry(country){
    this.errorMsg='';
    let countryPattern = /^[a-zA-Z]*$/
    if(country!=null && country!=''){
      let am = countryPattern.test(country);
      if(countryPattern.test(country)){
        return true;
      }else{
        this.errorMsg='Invalid country';
        return false;
      }
    }
  }
  validateCity(city){
    this.errorMsg='';
    let cityPattern = /^[a-zA-Z]*$/
    if(city!=null){
      let am = cityPattern.test(city);
      if(cityPattern.test(city)){
        return true;
      }else{
        this.errorMsg='Invalid city';
        return false;
      }
    }
  }
  validateState(state){
    this.errorMsg='';
    let statePattern = /^[a-zA-Z]*$/
    if(state!=null && state!=''){
      if(statePattern.test(state)){
        return true;
      }else{
        this.errorMsg='Invalid state';
        return false;
      }
    }
  }
  validateZip(zipCode){
    this.errorMsg='';
    let zipPattern=/^\d{5}(?:[-\s]\d{4})?$/;
    if(zipCode!=null && zipCode!=''){
      if(zipPattern.test(zipCode)){
        return true;
      }else{
         this.errorMsg='Invalid zip code';
         return false;
      }
    }
  }
}
