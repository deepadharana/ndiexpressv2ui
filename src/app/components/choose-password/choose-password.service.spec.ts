import { TestBed, inject } from '@angular/core/testing';

import { ChoosePasswordService } from './choose-password.service';

describe('ChoosePasswordService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChoosePasswordService]
    });
  });

  it('should be created', inject([ChoosePasswordService], (service: ChoosePasswordService) => {
    expect(service).toBeTruthy();
  }));
});
