import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { HttpService } from "../../common/http.service";
import { Response, Headers, URLSearchParams } from '@angular/http';

@Injectable()
export class ChoosePasswordService {
  constructor(private _httpService : HttpService) { }

  public setChoosePassword(resetToken: string, password : string, confirmPassword :string) : Observable <any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._httpService.post('ResetPassword', JSON.stringify({ResetToken : resetToken, Password : password, ConfirmPassword : confirmPassword }), {headers: headers})
      .map(res=><any>res.json())
      .catch(error=> this.handleError(error))
  }

  public validateToken(token: string) : Observable <any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const params = new URLSearchParams();
    params.set('token', token);

    return this._httpService.get('ValidateToken', {headers: headers, search: params})
      .map((res : Response) => res.json());
  }

  handleError(error : any){
    if(error instanceof Response){
      return Observable.throw(error.json()['error'] || 'backend server error');
    }
  }
}
