import { Component, OnInit } from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { ChoosePasswordService } from "./choose-password.service";
import { Router } from '@angular/router';
import { ApplicationService } from "../../common/application.service";
import { AppComponent } from "../app.component";

@Component({
  selector: 'app-choose-password',
  templateUrl: './choose-password.component.html',
  styleUrls: ['./choose-password.component.css'],
  providers: [ChoosePasswordService, Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class ChoosePasswordComponent implements OnInit {

  passwordField = '';
  confirmPasswordField = '';
  resetToken = '';
  capsOn = false;
  notMatching = false;
  wrongPattern = false;
  location: Location;
  tokenSuccess = '';
  tokenFailure = '';
  passwordSuccess = '';
  passwordFailure = '';
  confirmPasswordFailure = '';
  resetTokenFailure = '';
  alertMsg = false;

  constructor(private _choosePasswordService : ChoosePasswordService,
              location: Location,
              private _appService: ApplicationService,
              private _router: Router,
              private _app: AppComponent) { 
                this.location = location;
              }

  ngOnInit() {
    if(this._appService.currentUser != null){
      this._router.navigate(['/home']);
    }
    this.findToken();
  }

  capsLock(e) {
    let kCode = e.keyCode ? e.keyCode : e.which;
    let sKey = e.shiftKey ? e.shiftKey : ((kCode == 16) ? true : false);
    if (((kCode >= 65 && kCode <= 90) && !sKey) || ((kCode >= 97 && kCode <= 122) && sKey)) {
      this.capsOn = true;
    }
    else {
      this.capsOn = false;
    }
  }

  testPassword(password) {
    let Password_REGEX = /^(?=(.*?[A-Z]){2,})(?=(.*[a-z]){2,})(?=(.*[\d]){2,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/;
    if(! Password_REGEX.test(password)) {
      this.wrongPattern = true;
    }
    else {
      this.wrongPattern = false;
    }
  }

  findToken(){
    let tokenUrl = location.search.split('=');
    this.resetToken = tokenUrl[1];

    this._app.loading = true;
    this.alertMsg = true;
    this._choosePasswordService.validateToken(this.resetToken)
    .subscribe(response => {
        if (response.Success) {
          this._app.loading = false;
          this.tokenSuccess = response.Message;
        }
        else {
          this._app.loading = false;
          this.tokenFailure = response.Content.Errors.ResetLink;
        }
      })
  }

  close() {
    this.alertMsg = false;
  }

  updatePassword(password, confirmPassword) {  
    this._app.loading = true;  
    this.tokenSuccess = '';
    this.tokenFailure = '';  

    this._choosePasswordService.setChoosePassword(this.resetToken, password, confirmPassword)
      .subscribe(response => {
        if (response.Success) {
          this._app.loading = false;
          this.resetTokenFailure = '';
          this.passwordFailure = '';
          this.confirmPasswordFailure = '';
          this.notMatching = false;
          this.passwordSuccess = response.Message;
        }
        else {
          if(this.resetToken == '' || password == '' || confirmPassword == '') {
            this._app.loading = false;
            this.resetTokenFailure = response.Content.Errors.ResetToken;
            this.passwordFailure = response.Content.Errors.Password;
            this.confirmPasswordFailure = response.Content.Errors.ConfirmPassword;
            this.passwordSuccess = '';
            this.notMatching = false;
          }
          else {
            if (this.passwordField != this.confirmPasswordField) {
              this._app.loading = false;
              this.notMatching = true;
              this.resetTokenFailure = '';
              this.passwordFailure = '';
              this.confirmPasswordFailure = '';
              this.passwordSuccess = '';
            }
            else {
              this._app.loading = false;
              this.notMatching = false;
              this.resetTokenFailure = response.Content.Errors.ResetLink;
              this.passwordFailure = '';
              this.confirmPasswordFailure = '';
              this.passwordSuccess = '';
            }
          }
        }
      }    
    )}

}
