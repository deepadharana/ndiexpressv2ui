import { Component, OnInit } from '@angular/core';
import { Orders } from './data';
import { SavedOrder } from "../../models/saved-order";

@Component({
  selector: 'app-saved-orders',
  templateUrl: './saved-orders.component.html',
  styleUrls: ['./saved-orders.component.css']
})
export class SavedOrdersComponent implements OnInit {

  orders = [];
  mySavedOrders = true;
  customerSavedOrders = false;

  constructor() { 
    this.orders = Orders;
  }

  ngOnInit() {
  }

  mySavedOrderClicked() {
    this.mySavedOrders = true;
    this.customerSavedOrders = false;
  }

  customerSavedOrderClicked() {
    this.mySavedOrders = false;
    this.customerSavedOrders = true;
  }

}
