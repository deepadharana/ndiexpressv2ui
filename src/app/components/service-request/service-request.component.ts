import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, RequestOptions, Headers, Response } from '@angular/http';  
import { Customer } from '../../models/Customer';
import { ServiceRequest } from '../../models/servicereq';
import { FileContent } from '../../models/filecontent';
import { ServiceIssueRequest } from '../../models/serviceissuereq';
import { ServiceRequestService } from './service-request.service';
import { ShippingDetails } from '../../models/shipping-details';
import { ApplicationService } from "../../common/application.service";
import { AppComponent } from "../app.component";

@Component({
    selector: 'service-request',
    templateUrl: './service-request.component.html',
    styleUrls: ['./service-request.component.css'],
    providers: [ServiceRequestService]
  })

  export class ServiceRequestComponent implements OnInit {
    userErrorMessage: string;
    isUploadBtn: boolean = true;  
    customerId: number = 0;
    private base64textString:String="";
    fileExtensionMessage: string="";
    fileSizeMessage: string="";
    invalidEmail: string="";
    emptycontactperson: string="";
    successMessage: string="";
    errorMessage: string="";
    fileNumExceedError: string="";
    requestIssueErrorMessage: string="";
    blankIssueName: string="";
    blankIssueBrand: string="";
    blankIssueDetail: string="";
    blankIssueLoc: string="";
    selectedShippingDetails = new ShippingDetails();
    issues: ServiceIssueRequest[] = [ new ServiceIssueRequest() ];
    filecontents: FileContent[] = [ ];
    servicereq: ServiceRequest = new ServiceRequest();
    attachedImageNames: string[]=[];
    attachedImageNamesStr: string = '';
    img: FileContent = new FileContent();
    alertMsg = false;
    custAlertMsg = false;
    imageSourceToMagnify = '';
    magnifyTheImage = false;
    emailEmpty: string = '';

    fileContent: FileContent={
      AttachmentContentBytes: "",
      AttachmentType:"",
      AttachmentName:"",
      ImageSource:"",
      ImageUniqueId: 0
    }

    customer: Customer = {
      CustomerId: 0,
      CustomerName: '',
      ShipToLocation: [ new ShippingDetails() ],
      OfficePhone: '',
      OfficialEmail: '',
      SalesRepId: 0,
      BranchMailId: '',
      AssignedSalesEmailId: '',
      Type: '',
      AccountRep: null,
      HomeOffice:null,
      Branch: null,
      ContactPerson: '',
      FirstName : '',
      LastName : '',
      Title : '',
      ContactNumber  : '',
      PracticeName : '',
      RePassword : '',
      FaxPhone : '',
      EmailAddress : '',
      Password : '',
      NdiAccount : 0,
      PracticeEmailId : '',
      BillToContactName  : '',
      ShipToContactName :'',
      ShipToLocationValue : ''
    };
  
    constructor( private serviceRequestService: ServiceRequestService,
                 private route: ActivatedRoute,
                 private router: Router,
                 private _appService: ApplicationService,
                 private _app: AppComponent) {
      this.selectedShippingDetails = this.customer.ShipToLocation[0];
      this.selectedShippingDetails.ShippingId = this.customer.ShipToLocation[0].ShippingId;
      this.customer.ShipToLocationValue = this.customer.ShipToLocation[0].ShippingName;
    }
  
    ngOnInit(): void {
      if(this._appService == undefined){
        this.router.navigate(['/login']);
      }
      this.custAlertMsg = false;;
      localStorage.setItem('servicerequest', JSON.stringify(this.servicereq));
      if(this._appService.currentUser.Account == null)
        {
          let cust = JSON.parse(localStorage.getItem('selected-customer-detail'));
          if(cust != undefined && cust != "null") {
            this.customerId = cust.CustomerId;
          }
          else{
            this.userErrorMessage = "Please select the customer";
            this.custAlertMsg = true;
          }
        }
        else{
          this.customerId = this._appService.currentUser.Account.CustomerId;
        }
        let loggedInUser = JSON.parse(localStorage.getItem('currentUser'));
        
      this.serviceRequestService.getCustomer(this.customerId.toString())
      .subscribe(data => {this.customer = data.Content.Result;
        this.customer.ShipToLocation = data.Content.Result.ShippingAddress;
        this.customer.OfficePhone = data.Content.Result.PhoneNumber;
        this.selectedShippingDetails = this.customer.ShipToLocation[0];
        this.selectedShippingDetails.ShippingId = this.customer.ShipToLocation[0].ShippingId;
        this.customer.OfficePhone = this.selectedShippingDetails.PhoneNumber;
        this.customer.ShipToLocationValue = this.customer.ShipToLocation[0].ShippingName;
        for(var i=0; i<data.Content.Result.ShippingAddress.length; i++){
          this.customer.ShipToLocation[i].ShippingName = data.Content.Result.ShippingAddress[i].PrimaryAddress +
                                                         (data.Content.Result.ShippingAddress[i].City == '' ? ' ': ', ') + data.Content.Result.ShippingAddress[i].City + 
                                                         (data.Content.Result.ShippingAddress[i].State == '' ? ' ': ', ') + data.Content.Result.ShippingAddress[i].State 
        }
        this.router.navigateByUrl('/service-request');
        });
    }

    emailCheck(email){
      let EMAIL_REGEXP = /([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/; 
     if(email != '' && email!=null && (email.length<=5 || !  EMAIL_REGEXP.test(email))){
      this.emailEmpty = '';
      window.scrollTo(0,0);
       this.invalidEmail='Invalid Email';
     }else{
       this.customer.EmailAddress = email;
       this.invalidEmail='';
     }
     if(email == ''){
      window.scrollTo(0,0);
       this.emailEmpty = 'Blank Field';
     }
     else{
       this.emailEmpty = '';
     }
    }

    contactPersonCheck(contactPerson){
     this.emptycontactperson = '';
      if(contactPerson == '' || contactPerson == undefined){
        window.scrollTo(0,0);
        this._app.loading = false;
        this.emptycontactperson = 'Blank Field'
      }
    }

    issueNameCheck() {this.blankIssueName = '';}

    issueBrandCheck() {this.blankIssueBrand = '';}

    issueDetailCheck() {this.blankIssueDetail = '';}

    issueLocCheck() {this.blankIssueLoc = '';}

    trackBy(index) {return index;}

    close() {
      this.alertMsg = false;
      this.custAlertMsg = false;
    }

    magnifyImage(source) {
      this.magnifyTheImage = true;
      this.imageSourceToMagnify = source;
    }

    closeMagnifiedImage() {
      this.magnifyTheImage = false;
      this.imageSourceToMagnify = '';
    }

    addInfo() { 
      this.alertMsg = false;
      this.blankIssueName = '';
      this.blankIssueBrand = '';
      this.blankIssueDetail = ''; 
      this.blankIssueLoc = '';
      this.requestIssueErrorMessage = '';
      let length = this.issues.length;
      if(length > 0){
        let issueData = this.issues[length-1];
        if(issueData.Brand == undefined || issueData.Brand == "" || 
          issueData.IssueDetails == undefined || issueData.IssueDetails == "" ||
          issueData.ItemLocation == "" || issueData.ItemLocation == undefined ||
          issueData.ItemName == "" || issueData.ItemName == undefined){
            this.alertMsg = true;
            this.blankIssueName = issueData.ItemName == "" || issueData.ItemName == undefined ? "Blank Field" : "";
            this.blankIssueBrand = issueData.Brand == "" || issueData.Brand == undefined ? "Blank Field" : "";
            this.blankIssueDetail = issueData.IssueDetails == "" || issueData.IssueDetails == undefined ? "Blank Field" : "";
            this.blankIssueLoc = issueData.ItemLocation == "" || issueData.ItemLocation == undefined ? "Blank Field" : "";
        }
        else if(this.fileExtensionMessage == ""){
          this.issues.push(new ServiceIssueRequest()); 
        }
      }
    }

    deleteInfo(index) { 
      if(this.issues.length != 1){ this.issues.splice(index, 1) }
    }

    isInArray(array, word) {
      return array.indexOf(word.toLowerCase()) > -1;
    }

    filesizevalidation() : boolean {
      let fileList: String[] = [ ];
      let filesize = 0;
      for(var i=0; i<this.servicereq.Issues.length; i++){
        if(this.servicereq.Issues[i].AttachedImages != undefined){
        for(var j=0; j<this.servicereq.Issues[i].AttachedImages.length; j++){
          fileList.push(this.servicereq.Issues[i].AttachedImages[j].AttachmentContentBytes);
          filesize = filesize + (this.servicereq.Issues[i].AttachedImages[j].AttachmentContentBytes.length);
        }
        filesize = filesize/(1000000);
      }
      }
      if(filesize <= 10){ return true; }
      else return false;
    }

    submitservicerequest() {
      this._app.loading = true;
      this.emailEmpty = '';
      this.alertMsg = false;
      this.fileExtensionMessage = '';
      this.blankIssueName = '';
      this.blankIssueBrand = '';
      this.blankIssueDetail = ''; 
      this.blankIssueLoc = '';
      this.errorMessage = '';
      this.successMessage = '';
      this.fileSizeMessage = '';
      this.requestIssueErrorMessage = '';
      this.contactPersonCheck(this.customer.ContactPerson);
      if(this.customer.EmailAddress == ''){
        this._app.loading = false;
        this.emailEmpty = 'Blank Field';
      }
      if(this.invalidEmail == '' && this.emptycontactperson == '' && this.emailEmpty == ''){
        this.setSeriveRequest();
        if(this.servicereq.Issues == undefined){
          this.issues = [ new ServiceIssueRequest() ];
          this.servicereq.Issues = this.issues;
          this.alertMsg = true;
          this._app.loading = false;
          this.requestIssueErrorMessage = "Sorry! There is no issue with your request. Please add one and submit";
        }
        if(this.issues.length >= 1){
        for(var k=0;k<this.issues.length;k++){
        let issueData = this.issues[k];
        if(issueData.Brand == undefined || issueData.Brand == "" || 
            issueData.IssueDetails == undefined || issueData.IssueDetails == "" ||
            issueData.ItemLocation == "" || issueData.ItemLocation == undefined ||
            issueData.ItemName == "" || issueData.ItemName == undefined){
            this.alertMsg = true;
            this._app.loading = false;
            this.blankIssueName = issueData.ItemName == "" || issueData.ItemName == undefined ? "Blank Field" : "";
            this.blankIssueBrand = issueData.Brand == "" || issueData.Brand == undefined ? "Blank Field" : "";
            this.blankIssueDetail = issueData.IssueDetails == "" || issueData.IssueDetails == undefined ? "Blank Field" : "";
            this.blankIssueLoc = issueData.ItemLocation == "" || issueData.ItemLocation == undefined ? "Blank Field" : "";
          }
          if(this.issues[k].AttachedImages != undefined){
            for(var j=0;j<this.issues[k].AttachedImages.length;j++){
              this.issues[k].AttachedImages[j].ImageUniqueId = undefined;
            }
          }
        }
      }
      if(this.servicereq.Issues.length != 0 && this.servicereq.Issues.length <= 10 && this.blankIssueName == ""  && this.blankIssueBrand == ""  && this.blankIssueDetail == ""  && this.blankIssueLoc == ""){
        if(this.filesizevalidation()){
          this.serviceRequestService.saveServiceRequest(this.servicereq)
          .subscribe(data => {
            this.resetservicerequest();
            this.alertMsg = true;
            this._app.loading = false;
            if(data.Success) {this.alertMsg = true; this.errorMessage = ''; this.successMessage = data.Message;}
            else{this.alertMsg = true; this.errorMessage = data.Message;}
          });
        }
        else {this._app.loading = false; this.alertMsg = true; this.fileSizeMessage = "Image size exceeded";}
        }
        else if(this.servicereq.Issues.length > 10){
          this.alertMsg = true;
          this._app.loading = false;
          this.requestIssueErrorMessage = 'This is your 11th issue, you can have multiple request as well';
        }
    }
    }

    resetservicerequest() {
      this.servicereq = new ServiceRequest();
      this.issues = [ new ServiceIssueRequest() ];
      this.servicereq.Issues = this.issues;
      this.customer.ContactPerson = '';
    }

    updateShipToLocation(){
      this.selectedShippingDetails = this.customer.ShipToLocation.filter(x=>x.ShippingName == this.customer.ShipToLocationValue)[0];
      this.servicereq.ShippingId = this.selectedShippingDetails.ShippingId;
      this.customer.OfficePhone = this.selectedShippingDetails.PhoneNumber;
    }

    setSeriveRequest(){
      this.servicereq = new ServiceRequest();
      this.servicereq.CustomerId = this.customer.CustomerId;
      this.servicereq.ShippingId = this.selectedShippingDetails.ShippingId;
      this.servicereq.OfficialEmail = this.customer.EmailAddress;
      this.servicereq.ContactPerson = this.customer.ContactPerson;
      let issueReq = new ServiceIssueRequest();
      if(!this.checkForEmptyIssue(this.issues[0]))
      { this.servicereq.Issues = this.issues; }
    }

    checkForEmptyIssue(issue){
      if(issue.ItemName == undefined && issue.Brand == undefined && issue.IssueDetails == undefined && issue.ItemLocation == undefined)
         return true;
      else return false;
    }

    removeImage(issue, image) {
      issue.AttachedImages = issue.AttachedImages.filter(x=>x.ImageUniqueId != image.ImageUniqueId);
      if(issue.AttachedImages.length == 0){
        this.attachedImageNamesStr = '';
      }
    }

    fileChange(event, issue, customer) { 
      this.alertMsg = false;
      this.attachedImageNames = [];
      this.fileExtensionMessage = '';
      this.errorMessage = '';
      this.successMessage = '';
      this.fileSizeMessage = '';
      this.requestIssueErrorMessage = '';
      let fileList: FileList = event.target.files;  
      this.filecontents = [];
      if(fileList.length > 5){
        this.alertMsg = true;
        this.fileNumExceedError = "A maximum of 5 images can be uploaded with each issue";
      }
      else if (fileList.length > 0) {  
        for(var i=0; i<fileList.length; i++){
          let file: File = fileList[i]; 

          let photoName = file.name;
          let fileExtensionError = false;

          var allowedExtensions = ["jpg","jpeg","png","bmp","gif"];
          let fileExtension = photoName.split('.').pop();

          if(this.isInArray(allowedExtensions, fileExtension)) {
            fileExtensionError = false;
              this.fileExtensionMessage = ""
          } else {
            this.alertMsg = true;
              this.fileExtensionMessage = "Please select mentioned format"
              fileExtensionError = true;
              event.target.files = new FileList();
          }

          if(!fileExtensionError){
            var reader = new FileReader();
            reader.readAsBinaryString(file);
            reader.onload = (fre:FileReaderEvent) => {
              var binaryString =  fre.target.result;
              this.base64textString = btoa(binaryString);
              this.fileContent.AttachmentContentBytes = this.base64textString;
              this.fileContent.AttachmentType = file.type;
              this.fileContent.AttachmentName = file.name;
              this.fileContent.ImageSource = "data:" + this.fileContent.AttachmentType + ";base64," + this.fileContent.AttachmentContentBytes;
              if(issue.AttachedImages == undefined){
                this.fileContent.ImageUniqueId = 0; this.filecontents.push(this.fileContent); issue.AttachedImages = this.filecontents;
              }
              else {
                if(issue.AttachedImages.length > 0){
                  this.fileContent.ImageUniqueId = issue.AttachedImages[issue.AttachedImages.length-1].ImageUniqueId + 1;
                }
                if(issue.AttachedImages.length < 5){
                  issue.AttachedImages.push(this.fileContent);
                }
                else{
                  this.alertMsg = true;
                  this.fileNumExceedError = "A maximum of 5 images can be uploaded with each issue";
                }
              }
              this.fileContent = new FileContent();
              this.base64textString = "";
              this.attachedImageNames.push(file.name);
              this.attachedImageNamesStr = '';
              this.attachedImageNamesStr = this.attachedImageNames.join(', ');
            }
          }
        }
        this.setSeriveRequest();
        this.servicereq.Issues = this.issues;
      }
    }
  }

  interface FileReaderEventTarget extends EventTarget {
    result:string
  }

  interface FileReaderEvent extends Event {
    target: FileReaderEventTarget;
    getMessage():string;
  }