import { Injectable } from '@angular/core';
import { ServiceRequest } from '../../models/servicereq';
import { Observable } from 'rxjs/Rx';
import { HttpService } from "../../common/http.service";
import { Response, Headers, URLSearchParams } from '@angular/http';

@Injectable()
export class ServiceRequestService {   
    constructor(private _httpService: HttpService){ }

    public getCustomer(custId: string): Observable<any> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
    
        const params = new URLSearchParams();
        params.set('customerId', custId);
    
        return this._httpService.get('CustomerById', {headers: headers, search: params})
          .map(res => <any>res.json())
          .catch(this.handleError);
    }

    public saveServiceRequest(servicereq: ServiceRequest) : Observable<any> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this._httpService.post("Service/RaiseRequest", JSON.stringify(servicereq), {headers: headers})
            .map(res => <any>res.json())
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        return Observable.throw('Server error');
    }
}