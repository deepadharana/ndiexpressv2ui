import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { HttpService } from "../../common/http.service";
import { Response, Headers, URLSearchParams } from '@angular/http';

@Injectable()
export class ResetPasswordService {
  constructor(private _httpServce : HttpService) { }

  public getForgetPassword(email: string) : Observable <any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const params = new URLSearchParams();
    params.set('userName', email);

    return this._httpServce.get('ForgetPassword', {headers: headers, search: params})
      .map((res : Response) => res.json())
      .catch(error=> this.handleError(error));
  }

  public getForgetUsername(accountId: string, yourName: string) : Observable <any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const params = new URLSearchParams();
    params.set('accountId', accountId);
    params.set('yourName', yourName);

    return this._httpServce.get('ForgetUserId', {headers: headers, search: params})
      .map((res : Response) => res.json());
  }

  handleError(error : any){
    if(error instanceof Response){
      return Observable.throw(error.json()['error'] || 'backend server error');
    }
  }
}
