import { Component, OnInit } from '@angular/core';
import { ResetPasswordService } from "./reset-password.service";
import { ApplicationService } from "../../common/application.service";
import { Router } from '@angular/router';
import { AppComponent } from "../app.component";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css'],
  providers: [ResetPasswordService]
})
export class ResetPasswordComponent implements OnInit {
  forgetUsername = false;
  forgetPassword = true;
  userLoginEmail = '';
  userName = '';
  userAccount: number;
  invalidAccountNumber = false;
  invalidEmail = false;
  capsOn = false;
  successMessage = '';
  failureMessage = '';
  usernameFailureMessage = '';
  accountFailureMessage = '';
  namesuccessMessage = '';
  alertMsg = false;
  headerInfo = false;

  constructor(private _resetPasswordService: ResetPasswordService,
    private _appService: ApplicationService,
    private _router: Router,
    private _app: AppComponent) { }

  ngOnInit() {
    if(this._appService.currentUser != null){
      this._router.navigate(['/home']);
    }
  }

  openInfo(){
    this.headerInfo = !this.headerInfo;
  }

  forgetUsernameClicked() {
    this.forgetUsername = true;
    this.forgetPassword = false;
  }

  forgetPasswordClicked() {
    this.forgetUsername = false;
    this.forgetPassword = true;
  }

  accountNumberCheck(value) {
    let AccNo_REGEX = /^[0-9]*\d$/;
    if(AccNo_REGEX.test(value)){
      this.invalidAccountNumber = false;
    }
    else {
      this.invalidAccountNumber = true;
    }
  }

  emailCheck(value){
    let EMAIL_REGEXP = /([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    if(EMAIL_REGEXP.test(value)){
      this.invalidEmail = false;
    }
    else {
      this.invalidEmail = true;
    }
  }

  capsLock(e) {
    let kCode = e.keyCode ? e.keyCode : e.which;
    let sKey = e.shiftKey ? e.shiftKey : ((kCode == 16) ? true : false);
    if (((kCode >= 65 && kCode <= 90) && !sKey) || ((kCode >= 97 && kCode <= 122) && sKey)) {
      this.capsOn = true;
    }
    else {
      this.capsOn = false;
    }
  }

  resetPassword(email) {
    this._app.loading = true;
    this.alertMsg = true;
    this._resetPasswordService.getForgetPassword(email)
    .subscribe(response => {
      if (response.Success) {
        this._app.loading = false;
        this.successMessage = response.Message;
        this.failureMessage = '';        
      }
      else {
        if(email == '') {
          this._app.loading = false;
          this.failureMessage = response.Content.Errors.UserName;
          this.successMessage = '';
        }
        else {
          this._app.loading = false;
          this.failureMessage = response.Content.Errors.Username;
          this.successMessage = '';
        }       
      }
    })
  }

  resetUsername(account, name) {
    this._app.loading = true;
    this.alertMsg = true;
    this._resetPasswordService.getForgetUsername(account == undefined ? 0 : account, name)
    .subscribe(response => {
      if (response.Success) {
        this._app.loading = false;
        this.namesuccessMessage = response.Message;
        this.usernameFailureMessage = '';
        this.accountFailureMessage = '';
      }
      else {
        if(account == undefined || name == '') {
          this._app.loading = false;
          this.accountFailureMessage = response.Content.Errors.AccountId;
          this.usernameFailureMessage = response.Content.Errors.YourName;
          this.namesuccessMessage = '';
        }
        else {
          if(response.Content.Errors.AccountId != undefined) {
            this._app.loading = false;
            this.accountFailureMessage = response.Content.Errors.AccountId;
            this.usernameFailureMessage = '';
            this.namesuccessMessage = '';
          }
          else {
            this._app.loading = false;
            this.accountFailureMessage = response.Content.Errors.AccountNumber;
            this.usernameFailureMessage = '';
            this.namesuccessMessage = '';
          }          
        }
      }
    })
  }

  close() {
    this.alertMsg = false;
  }

}
