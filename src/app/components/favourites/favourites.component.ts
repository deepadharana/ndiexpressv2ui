import { Component, OnInit} from '@angular/core';
import { FavouritesService } from './favourites.service';
import { Observable } from 'rxjs/Rx';
import { Persons } from '../item-history/data';
import { Availability } from '../item-history/data';
import { Brand } from '../item-history/data';
import { Categories } from '../item-history/data';
import { ItemHistory } from "../../models/item-history";

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css'],
  providers : [FavouritesService]
})
export class FavouritesComponent implements OnInit {

  noofpages = [];
  items = [];
  selectOption = []
  selectedValue: number;
  pageitem: string;
  limit: number;
  offset = 0;
  currentpagecount = 1;
  firstpagecount: number;
  lastpagecount: number;
  resultList = [];
  allSelected: any;
  brandSortDir: string = 'asc';
  descSortDir: string = 'asc';
  orderDateSortDir: string = 'asc';
  qtySortDir: string = 'asc';
  index: number;
  _listFilter: string;
  showHiddenItems: boolean = false;
  allResultList = [];
  filteredResultList = [];

  showItem = true;
  showDesc = true;
  showOnhand = true;
  showBrand = true;
  showCategory = true;
  showMonthQty = true;
  showDate = true;
  showQty = true;
  showPrice = true;

  availabilityList = [];
  brandList = [];
  categoryList = [];

  selectedAvailabilities = [];
  selectedBrands = [];
  selectedCategories = [];
  availabilities = [];

  showItemWithPromos: boolean = true;
  showFieldsOnMobile = false;
  promosFilter:boolean = false;
  showColumnTooltip = false;
  showMobileSearch = false;

  get listFilter(): string {
    return this._listFilter;
  }

  set listFilter(value: string) {
    this._listFilter = value;
    if(this.listFilter != undefined && this.listFilter != '') {
       this.resultList  = this.performFilter(this.listFilter);
      } 
      else {
        this.itemsperpage();
         if(this.showHiddenItems){
          this.resultList = this.resultList.filter(x=>!x.IsVisible);
        }
        else{
          this.resultList = this.resultList.filter(x=>x.IsVisible);
        }
      }
  }

  constructor(private favouriteService :  FavouritesService) {
    let exist = false;
    this.items = Persons;
    // this.items.forEach(function (eachitem, key) {
    //   if(this.availabilities.length > 0){
    //       this.availabilities.forEach(function (x, key) {
    //         if(eachitem.availability == x) exist = true;
    //     });
    //     if(!exist){ 
    //       this.availabilities.push(eachitem);
    //     }
    //     exist = false;
    //   }
    // });
    // this.availabilities.forEach(function () {
      
    // })
    this.selectOption = [1,2,3,4,5,6,7,8,9,10,11,12];
    this.selectedValue = this.selectOption[4];
    this.pageitem = this.selectedValue.toString();
    this.limit = parseInt(this.pageitem);
    this.firstpagecount = 1;
    this.lastpagecount = Math.ceil(this.items.length / this.limit);
    this.itemsperpage();
    for(let i=1; i<=this.lastpagecount; i++) {
      this.noofpages.push(i);
    }
    this.availabilityList = Availability;
    this.brandList = Brand;
    this.categoryList = Categories;
    this.itemsperpage();
   }

  ngOnInit() {
  }

  showItemColumn() {
    this.showItem = !this.showItem;
  }

  showDescColumn() {
    this.showDesc = !this.showDesc;
  }

  showOnhandColumn() {
    this.showOnhand = !this.showOnhand;
  }

  showBrandColumn() {
    this.showBrand = !this.showBrand;
  }

  showCategoryColumn() {
    this.showCategory = !this.showCategory;
  }

  showMonthQtyColumn() {
    this.showMonthQty = !this.showMonthQty;
  }

  showDateColumn() {
    this.showDate = !this.showDate;
  }

  showQtyColumn() {
    this.showQty = !this.showQty;
  }

  showPriceColumn() {
    this.showPrice = !this.showPrice;
  }

  showMoreFields() {
    this.showFieldsOnMobile = !this.showFieldsOnMobile;
  }

  toggleColumnTooltip() {
    this.showColumnTooltip = !this.showColumnTooltip;
  }

  toggleMobileSearch(){
    this.showMobileSearch = !this.showMobileSearch;
  }

  closeMobileSearch() {
    this.showMobileSearch = false;
  }

  performFilter(filterBy: string): any[] {
    this.resultList =  this.allResultList;

    //search start
    if(filterBy != undefined){
      filterBy = filterBy.toLocaleLowerCase();
      this.resultList =  this.allResultList.filter((item: any) =>
        (item.Brand.toLocaleLowerCase().indexOf(filterBy) !== -1) ||
        item.ItemId.toString().indexOf(filterBy) !== -1 ||
        item.Desc.toLocaleLowerCase().indexOf(filterBy) !== -1);
    }
    //search end
    
    //filtering start
    if(this.selectedAvailabilities.length > 0){
      this.resultList = this.resultList.filter(item => this.selectedAvailabilities.some(f => f == item.Availability));
    }

    if(this.selectedBrands.length > 0){
      this.resultList = this.resultList.filter(item => this.selectedBrands.some(f => f == item.Brand));
    }

    if(this.selectedCategories.length > 0){
      this.resultList = this.resultList.filter(item => this.selectedCategories.some(f => f == item.Category));
    }

    if(this.promosFilter){
      this.resultList = this.resultList.filter(x=>x.Promos.length > 0);
    }
    //filtering end

    //Show/Hide items start
    if(this.showHiddenItems){
      this.resultList = this.resultList.filter(x=>!x.IsVisible);
    }
    else{
      this.resultList = this.resultList.filter(x=>x.IsVisible);
    }
    //Show/Hide items end

    return this.resultList;
  }

  orderBy(column, direction) {
    if(direction == 'asc'){
      this.resultList = this.resultList.sort((a: any, b: any) => {
        if ( a[column] < b[column] ){
          return -1;
        }else if( a[column] > b[column] ){
            return 1;
        }else{
          return 0;	
        }
      });
      this.brandSortDir = column == 'brand' ? 'desc' : this.brandSortDir;
      this.descSortDir = column == 'desc' ? 'desc' : this.descSortDir;
      this.orderDateSortDir = column == 'date' ? 'desc' : this.orderDateSortDir;
      this.qtySortDir = column == 'quantity' ? 'desc' : this.qtySortDir;
    }
    else{
      this.resultList = this.resultList.sort((a: any, b: any) => {
        if ( a[column] > b[column] ){
          return -1;
        }else if( a[column] < b[column] ){
            return 1;
        }else{
          return 0;	
        }
      });
      this.brandSortDir = column == 'brand' ? 'asc' : this.brandSortDir;
      this.descSortDir = column == 'desc' ? 'asc' : this.descSortDir;
      this.orderDateSortDir = column == 'date' ? 'asc' : this.orderDateSortDir;
      this.qtySortDir = column == 'quantity' ? 'asc' : this.qtySortDir;
    }
  }

  toggleSort(x){
    x.classList.toggle("fa fa-sort-asc");
  }

  changedPageItem(event){
    this.currentpagecount = 1;
    this.noofpages = [];
    this.pageitem = this.selectOption[event.target.selectedIndex];    
    this.limit = parseInt(this.pageitem);
    this.lastpagecount = Math.ceil(this.items.length / parseInt(this.pageitem));    
    for(let i=1; i<=this.lastpagecount; i++) {
      this.noofpages.push(i);
    }
    this.itemsperpage();
  }

  pageBack() {
    this._listFilter = '';
    if(this.currentpagecount > this.firstpagecount) {
      this.currentpagecount -= 1;      
    }
    this.itemsperpage();
  }

  pageForward() {
    this._listFilter = '';
    if(this.currentpagecount < this.lastpagecount && this.currentpagecount >= this.firstpagecount) {
      this.currentpagecount += 1;        
    }
    this.itemsperpage();
  }

  itemsperpage() {
    this.allResultList =[];
    for(let i=((this.currentpagecount * this.limit)-(this.limit - 1)); i <= (this.currentpagecount * this.limit); i++) {
      if(i <= this.items.length)
        this.allResultList.push(this.items[i-1]);
    }
    this.orderBy('desc', 'asc');
    this.offset = (this.currentpagecount * this.limit)-(this.limit);
    let n = (Math.min(this.currentpagecount * this.limit, this.items.length));
    this.pageitem = n.toString();

    this.singleCheck();
    if(this.showHiddenItems){
      this.resultList = this.allResultList.filter(x=>!x.IsVisible);
    }
    else{
      this.resultList = this.allResultList.filter(x=>x.IsVisible);
    }
  }

  selectAll() {
    let toggleStatus = !this.allSelected;
    this.resultList.forEach(function (eachitem, key) {
      eachitem.selected = toggleStatus;
    });
  }

  singleCheck() {
    this.allSelected = this.resultList.every(function(itm){ return itm.selected; });
  }

  pageClick(i) {
    this._listFilter = '';
    this.index = i + 1;
    this.currentpagecount = this.index;
    this.itemsperpage();    
  }

  hideShowItems() {
    let hiddenItemList = this.allResultList.filter(x=>!x.IsVisible);
    let visibleItemList = this.allResultList.filter(x=>x.IsVisible);
    if(this.allResultList != undefined){
      this.allResultList.forEach(function (eachitem, key) {
        if(eachitem.Selected){
          eachitem.IsVisible = !eachitem.IsVisible;
          if(eachitem.IsVisible){
            hiddenItemList = hiddenItemList.filter(pop => pop.ItemId != eachitem.ItemId)
            visibleItemList.push(eachitem);
          }
          else{
            hiddenItemList.push(eachitem);
            visibleItemList = visibleItemList.filter(pop => pop.ItemId != eachitem.ItemId)
          }
        }
        eachitem.selected = false;
      }); 
    }
    if(this.showHiddenItems){
      this.resultList = hiddenItemList;
    }
    else{
      this.resultList = visibleItemList;  
    }
    if(this.resultList.length == 0) this.allSelected = false;
    if(this.listFilter != undefined) this.performFilter(this.listFilter);
  }

  toggleVisibility() {
    this.allSelected = false;
    this.showHiddenItems = !this.showHiddenItems;
    
    if(this.showHiddenItems){
      this.resultList = this.allResultList.filter(x=>!x.IsVisible);
    }
    else{
      this.resultList = this.allResultList.filter(x=>x.IsVisible);
    }
    
    this.resultList.forEach(function (eachitem, key) {
        eachitem.Selected = false;
    });  

    if(this.listFilter != undefined) this.performFilter(this.listFilter);
  }

  updateSelectedAvailabilities(availability, action){
    if(action == 'select'){
      this.selectedAvailabilities.push(availability.text);
    }
    else{
      this.selectedAvailabilities = this.selectedAvailabilities.filter(x=>x!=availability.text);
    }
  }

  updateSelectedBrands(brand, action){
    if(action == 'select'){
      this.selectedBrands.push(brand.text);
    }
    else{
      this.selectedBrands = this.selectedBrands.filter(x=>x!=brand.text);
    }
  }

  updateSelectedCategories(category, action){
    if(action == 'select'){
      this.selectedCategories.push(category.text);
    }
    else{
      this.selectedCategories = this.selectedCategories.filter(x=>x!=category.text);
    }
  }

  showHidePromos() {
    this.showItemWithPromos = !this.showItemWithPromos;
  }

  applyFilters(){
    this.performFilter(this.listFilter);
  }

  showPromos() {
    this.promosFilter = !this.promosFilter;
  }

  searchEventHandler() {
    this.performFilter(this.listFilter);
    this.allSelected = false;
  }
  
  showExtdDesc(item, action) {
    if(this.resultList != undefined){
      let updateItem = this.resultList.filter(x=>x.ItemId == item);
      let index = this.resultList.indexOf(updateItem);
      if(action == "more") item.IsExtDescVisible = true;
      else item.IsExtDescVisible = false;
      this.resultList[index] = item;
    }
  }

}
