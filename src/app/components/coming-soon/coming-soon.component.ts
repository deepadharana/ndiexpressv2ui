import { Component, OnInit } from '@angular/core';
import { ApplicationService } from "../../common/application.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-coming-soon',
  templateUrl: './coming-soon.component.html',
  styleUrls: ['./coming-soon.component.css']
})
export class ComingSoonComponent implements OnInit {

  constructor(private _appService :  ApplicationService,private router : Router) { }

  ngOnInit() {
  }

  submit(){
    this._appService.navigate("coming-soon","change-password");
  }
  cancel(){
      this.router.navigate(['/'+this._appService.callbackUrl]);
  }
}
