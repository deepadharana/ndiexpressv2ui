import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ApplicationService } from "../../common/application.service";

@Component({
  selector: 'app-customer-information',
  templateUrl: './customer-information.component.html',
  styleUrls: ['./customer-information.component.css']
})
export class CustomerInformationComponent implements OnInit {

  accountIdEntered:boolean=false;

  constructor( private _appService : ApplicationService,
    private router: Router) { }

  ngOnInit(){
    if(this._appService.currentUser==null){
    this.router.navigate(['/login']);
   }
  }
  

  accountIdentered(){
    this.accountIdEntered= true;
  }

}