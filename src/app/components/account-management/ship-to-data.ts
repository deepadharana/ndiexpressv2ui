import { ShipToAddress } from "./ship-to-address";

export const ShippingAddress: ShipToAddress[] = [
    {
        AddressId : 1,
        ContactAddress : "Compunnel Office, Logix Cyber Park, Noida - 62",
        ContactName : "Dr. Cersai Lannister",
        ZipCode : "102136",
        Selected : false
    },
    {
        AddressId : 2,
        ContactAddress : "Compunnel Office, Steller IT Park, Noida - 63",
        ContactName : "Dr. Cersai Lannister",
        ZipCode : "102137",
        Selected : false
    },
    {
        AddressId : 3,
        ContactAddress : "Compunnel Office, Logix Park, Noida - 65",
        ContactName : "Dr. Cersai Lannister",
        ZipCode : "102138",
        Selected : false
    }
]