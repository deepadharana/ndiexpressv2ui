import { Component, OnInit } from '@angular/core';
import { AccountManagementService } from "./account-management.service";
import { NewCustomerData } from "../../models/newCustomerData";
import { Router } from "@angular/router";
import { ApplicationService } from "../../common/application.service";
import { NewPermission } from "../add-new-role/permission-data";
import { ShippingAddress } from "./ship-to-data";

@Component({
  selector: 'app-account-management',
  templateUrl: './account-management.component.html',
  styleUrls: ['./account-management.component.css'],
  providers : [AccountManagementService]
})
export class AccountManagementComponent implements OnInit {

  customerSelected:boolean = true;
  NDIUserSelected:boolean = false;
  newCustAdd:boolean = false;
  newCustomerData: NewCustomerData[] = [];
  errorMessage: string;
  selectedNum: number = 1;
  permissions=[];
  status=true;
  permissionStatus=true;
  isAccountIdEntered: boolean =false;
  selectedAll : any;
  shippingAddress=[];

  constructor(private _accountmanagementService : AccountManagementService,
              private _appService : ApplicationService,
              private router: Router) {
                this.permissions=NewPermission;
                this.shippingAddress=ShippingAddress;
               }

  ngOnInit() {
    if(this._appService.currentUser==null){
      this.router.navigate(['/login']);
     }
    this._accountmanagementService.getNewCustomers()
    .subscribe(
    newcustomer => {
      this.newCustomerData = newcustomer
     },
    error => this.errorMessage = <any>error
    );
  }
  displayData(number) {
    this.selectedNum = number;
  }

  customerSelectedClicked() {
    this.customerSelected = true;
    this.NDIUserSelected = false;
  }

  NDIUserSelectedClicked() {
    this.customerSelected = false;
    this.NDIUserSelected = true;
  }
  newCustomerClicked(){
    this.newCustAdd=true;
  }

  checkPersonalInfoExpandStatus(){
    this.status=!this.status;
  }
  checkPermissionsExpandStatus(){
    this.permissionStatus=!this.permissionStatus;
  }
  accountIdEntered(){
    this.isAccountIdEntered= true;
  }
  cancel(){
    this.newCustAdd=false;
  }
  selectAll(){
    for(var i=0;i<this.shippingAddress.length;i++){
      this.shippingAddress[i].Selected=this.selectedAll;
    }
  }
  checkIfAllSelected(){
    this.selectedAll = this.shippingAddress.every(function(item : any){
      return item.Selected==true;
    });
  }
 
}
