import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { NewCustomerData } from "../../models/newCustomerData";

@Injectable()
export class AccountManagementService {
  private _productUrl = "../assets/mock-data/newCustomer.json";
  
       constructor(private _http : Http){}
 
      getNewCustomers() : Observable<NewCustomerData[]>{
     
        return this._http.get(this._productUrl)
        .map((response : Response)=><NewCustomerData[]>response.json())
        .catch(this.handleError);
      }
     
     private handleError(err : Response) {
       return Observable.throw(err.json().err || " Server error");
     }

}
