export class ShipToAddress{
    AddressId : number;
    ZipCode : string;
    ContactName : string;
    ContactAddress : string;
    Selected : boolean;
}