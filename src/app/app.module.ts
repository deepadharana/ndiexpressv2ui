
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Router } from '@angular/router';

/************** Helpers ******************** */

import { FilterPipe } from './common/pipes/filter.pipe';
import { AppRouteModule, routing } from './app.routes';
import { Configuration } from "../assets/config/config";

/*********  Custom Modules *************** */

import { LoadingModule } from 'ngx-loading';
import { SelectModule } from 'ng2-select';
import { Http, XHRBackend, RequestOptions } from '@angular/http';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { MomentModule } from 'angular2-moment';
import { ConfigModule, ConfigLoader, ConfigStaticLoader } from '@ngx-config/core';

/************* Services used ************************* */

import { ApplicationService } from './common/application.service';
import { HttpService } from './common/http.service';
import { LoginService } from "../app/components/login/login.service";

/********* Page wise component ***** */

import { AppComponent } from './components/app.component';
import { LoginComponent } from './components/login/login.component';
import { LandingComponent } from './components/landing/landing.component';
import { ChoosePasswordComponent } from "./components/choose-password/choose-password.component";
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ResetPasswordComponent } from "./components/reset-password/reset-password.component";
import { TemplateHeaderComponent } from './common/template/template-header/template-header.component';
import { TemplateFooterComponent } from './common/template/template-footer/template-footer.component';
import { TemplateLeftSideComponent } from './common/template/template-left-side/template-left-side.component';
import { ServiceRequestComponent } from "./components/service-request/service-request.component";
import { MyAccountComponent } from './components/my-account/my-account.component';
import { AccountDetailsComponent } from './components/account-details/account-details.component';
import { SalesDashboardComponent } from './components/sales-dashboard/sales-dashboard.component';
import { ComingSoonComponent } from './components/coming-soon/coming-soon.component';
import { SearchCustomerComponent } from './components/search-customer/search-customer.component';
import { ItemHistoryComponent } from './components/item-history/item-history.component';
import { FavouritesComponent } from "./components/favourites/favourites.component";
import { CustomerRegistrationComponent } from './components/customer-registration/customer-registration.component';
import { BreadcrumbComponent } from "./common/template/breadcrumb/breadcrumb.component";
import { UserMaintenanceComponent } from './components/user-maintenance/user-maintenance.component';
import { CustomerRequestComponent } from './components/customer-request/customer-request.component';
import { AccountManagementComponent } from './components/account-management/account-management.component';
import { RolesAndPermissionComponent } from './components/roles-and-permission/roles-and-permission.component';
import { ItemDetailComponent } from './components/item-detail/item-detail.component';
import { AddNewRoleComponent } from "./components/add-new-role/add-new-role.component";
import { SavedOrdersComponent } from './components/saved-orders/saved-orders.component';
import { ItemSearchComponent } from './components/item-search/item-search.component';
import { CustomerInformationComponent } from './components/customer-information/customer-information.component';
import { PermissionsComponent } from './components/permissions/permissions.component';

export function configFactory(): ConfigLoader {
  return new ConfigStaticLoader({
    Configuration    
  });
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ServiceRequestComponent,
    CustomerRegistrationComponent,
    LandingComponent,
    ResetPasswordComponent,
    ChoosePasswordComponent,
    ChangePasswordComponent,
    TemplateHeaderComponent,
    TemplateFooterComponent,
    TemplateLeftSideComponent,
    MyAccountComponent,
    AccountDetailsComponent,
    SalesDashboardComponent,
    ComingSoonComponent,
    SearchCustomerComponent,
    FilterPipe,
    ItemHistoryComponent,
    FavouritesComponent,
    BreadcrumbComponent,
    UserMaintenanceComponent,
    CustomerRequestComponent,
    AccountManagementComponent,
    RolesAndPermissionComponent,
    ItemDetailComponent,
    AddNewRoleComponent,
    SavedOrdersComponent,
    ItemSearchComponent,
    CustomerInformationComponent,
    PermissionsComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRouteModule,
    LoadingModule,
    SelectModule,
    MomentModule,
    NgIdleKeepaliveModule.forRoot(),
    routing,
    ConfigModule.forRoot({
      provide: ConfigLoader,
      useFactory: (configFactory)
    })
  ],

  bootstrap: [AppComponent]
})

export class AppModule { }