import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Http, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../environments/environment';
import { ApplicationService } from "./application.service";
import { ConfigService } from '@ngx-config/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class HttpService extends Http {
    constructor (backend: XHRBackend, options: RequestOptions, private router: Router, private readonly config: ConfigService) {
        super(backend, options);
    }

    request(url: string|Request, options?: RequestOptionsArgs): Observable<Response> {
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let token: string;
        if (currentUser != undefined && currentUser != null && (localStorage.getItem('isUserTimedout') == null || localStorage.getItem('isUserTimedout') == "false")) {
            token = currentUser.AccessToken;
        }
        else{
            token = 'bmRpZXhwcmVzc3Yy';
        }
        if (typeof url === 'string') { // meaning we have to add the token to the options, not in url
          if (!options) {
            // let's make option object
            options = {headers: new Headers()};
          }
          options.headers.set('ndi-authorization', token);
        } else {
            // we have to add the token to the url object
            //url.url = environment.apiOrigin + url.url;

            // url created using ngx config

            let configurl:string = this.config.getSettings('Configuration.system.applicationUrl');
            url.url = configurl + url.url;

            url.headers.set('ndi-authorization', token);
        }
        //console.log("Interceptor: ", url, options); 
        return super.request(url, options).catch(this.catchAuthError(this));
      }

    private catchAuthError (self: HttpService) {
        // we have to pass HttpService's own instance here as `self`
        return (res: Response) => {
           // console.log(res);
            if (res.status === 401 || res.status === 403) {
                // if not authenticated
              //  console.log(res);
                this.router.navigate(['/login']);
            }
            return Observable.throw(res);
        };
    }
}