import { Customer } from "../../models/Customer";
import { BillingDetails } from "../../models/billing-details";
import { ShipToDetails } from "../../models/ship-to-details";

export class Validation {
    errorMsg='';
    model =new Customer();
    modelBilling =new BillingDetails();
    modelShipDetails=new ShipToDetails();
    
    // validate email address
    validateEmail(email){
      this.errorMsg='';
      let emailPattern = /([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
      if(email!=null){
        if(email.length<255){
          if(emailPattern.test(email)){
            return true;
          }else{
            this.errorMsg='Invalid email';
            return false;
          }
        }else{
          this.errorMsg='Length of string is too large.';
          
          return false;
        }
      }else{
      }
    }
    // validate password
    validatePassword(password){
      this.errorMsg='';
      let passwordPattern = /^(?=(.*?[A-Z]){2,})(?=(.*[a-z]){2,})(?=(.*[\d]){2,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/;
      if(password!=null){
        let am = passwordPattern.test(this.model.Password);
        if(passwordPattern.test(this.model.Password)){ 
          return true;
        }else{
          this.errorMsg='Invalid password';
          return false;
        }
      }else{
  
      }
    }
    // validate first name
    validateFName(name){
      this.errorMsg='';
      let namePattern = /^[\sa-zA-Z\s]*$/
      if(name!=null && name!=''){
        let am = namePattern.test(name);
        if(name.length<15){
          if(namePattern.test(name)){
            return true;
          }else{
            this.errorMsg='Invalid name';
            return false;
          }
        }else{
          this.errorMsg='Invalid name';
          return false;
        }
      }else{
        //this.errorMsg='can not be empty';
        return false;
      }
    }
    // validate last name
    validateLName(name){
      this.errorMsg='';
      let namePattern = /^[\sa-zA-Z\s]*$/
      if(name!=null && name!=''){
        let am = namePattern.test(name);
        if(name.length<25){
          if(namePattern.test(name)){
            return true;
          }else{
            this.errorMsg='Invalid name';
            return false;
          }
        }else{
          this.errorMsg='Invalid name';
          return false;
        }
      }else{
        //this.errorMsg='can not be empty';
        return false;
      }
    }
    // validate name
    validateName(name){
      this.errorMsg='';
      let namePattern = /^[\sa-zA-Z\s]*$/
      if(name!=null && name!=''){
        let am = namePattern.test(name);
        if(name.length<40){
          if(namePattern.test(name)){
            return true;
          }else{
            this.errorMsg='Invalid name';
            return false;
          }
        }else{
          this.errorMsg='Invalid name';
          return false;
        }
      }else{
        //this.errorMsg='can not be empty';
        return false;
      }
    }
    //validate number
    validateNumber(contactNumber){
      this.errorMsg='';    
      let contactNumberPattern=/^[0-9]{10}$/
      if(contactNumber!=null){
        let am=contactNumberPattern.test(contactNumber);
        if(contactNumber.length==10){
          if(contactNumberPattern.test(contactNumber)){ 
            return true;
          }else{
            this.errorMsg='Invalid number';
            return false;  
          }
        }else{ 
          this.errorMsg='Invalid number';
          return false;
        }
      }else{
        //this.errorMsg='Contact number can not be empty';
      }
    }
    // validate fax number
    validateFax(faxNumber){
      this.errorMsg='';    
      let faxNumberPattern=/^[0-9]{10}$/
      if(faxNumber!=null){
        let am=faxNumberPattern.test(faxNumber);
        if(faxNumber.length==10){
          if(faxNumberPattern.test(faxNumber)){
            return true;
          }else{
            this.errorMsg='Invalid fax number';
            return false;  
          }
        }else{ 
          this.errorMsg='Invalid fax number';
          return false;
        }
      }else{
        //this.errorMsg='Contact number can not be empty';
      }
    }  
    // validate confirm password
    validateConfirmPassword(_password,reEnteredPassword){
      this.errorMsg='';    
      if(_password!=null && reEnteredPassword!=null){
        if(_password.trim()==reEnteredPassword.trim()){
          return true;
        }else{
          this.errorMsg='Password does not match';
          return false;
        }
      }else{
        //this.errorMsg='This field can not be empty';
      }
    }
    // validate NDI account
    validateNdi(ndiAccount){
      this.errorMsg='';    
      let faxNumberPattern=/^[0-9]{10}$/
      if(ndiAccount!=null){
        let am=faxNumberPattern.test(ndiAccount);
        if(ndiAccount.length==10){
          if(faxNumberPattern.test(ndiAccount)){
            return true;
          }else{
            this.errorMsg='Invalid Ndi number';
            return false;  
          }
        }else{ 
          this.errorMsg='Invalid Ndi number';
          return false;
        }
      }else{
        //this.errorMsg='Contact number can not be empty';
      }
    }
    // show password policy
    /*showPasswordPolicy(){
      this.errorMsg='';
      let _enteredPassword=this.model.Password;
      if(_enteredPassword!=null){
        if(_enteredPassword.length>0){
          this.passwordRules=true;
          
          return true;
        }else{
          this.passwordRules=false;
          return false;
        }
      }else{
  
      }
    }*/
    // validate city
    validateCountry(country){
      this.errorMsg='';
      let countryPattern = /^[a-zA-Z]*$/
      if(country!=null){
        let am = countryPattern.test(country);
        if(country.length<20){
          if(countryPattern.test(country)){
            return true;
          }else{
            this.errorMsg='Invalid country name';
            return false;
          }
        }else{
          this.errorMsg='Invalid country name';
          return false;
        }
      }else{
      }
    }
    // validate city
    validateCity(city){
      this.errorMsg='';
      let cityPattern = /^[a-zA-Z]*$/
      if(city!=null){
        let am = cityPattern.test(city);
        if(city.length<20){
          if(cityPattern.test(city)){
            return true;
          }else{
            this.errorMsg='Invalid city';
            return false;
          }
        }else{
          this.errorMsg='Invalid city';
          return false;
        }
      }else{
      }
    }
    // validate state
    validateState(state){
      this.errorMsg='';
      let statePattern = /^[a-zA-Z]*$/
      if(state!=null){
        if(state.length<20){
          if(statePattern.test(state)){
            return true;
          }else{
            this.errorMsg='Invalid state';
            return false;
          }
        }else{
          this.errorMsg='Invalid state';
          return false;
        }
      }else{
      }
    }
    // validate postal code
    validateZip(zipCode){
      let errorMsg='';
      let zipPattern=/^\d{5}(?:[-\s]\d{4})?$/;
      if(zipCode!=null){
        if(zipCode.length==5){
          if(zipPattern.test(zipCode)){
            return true;
          }else{
            this.errorMsg='Invalid zip';
            return false;
          }
        }else{
          this.errorMsg='Invalid zip';
          return false;
        }
      }
    }
}