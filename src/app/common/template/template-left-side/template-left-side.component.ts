import { Component, OnInit } from '@angular/core';
import { ApplicationService } from "../../application.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-template-left-side',
  templateUrl: './template-left-side.component.html',
  styleUrls: ['./template-left-side.component.css']
})
export class TemplateLeftSideComponent implements OnInit {
  clicked : any;

  constructor(private _appService : ApplicationService, private router: Router,) { }

  ngOnInit() {
    this._appService.notifyObservable$.subscribe((response: any) => {
      this.clicked=response.data;
    });
  }

  toggleSidebar() {
    this.clicked = ! this.clicked;
  }

  opentoggleSidebar() {
    this.clicked = true;
  }

  closetoggleSidebar() {
    this.clicked = false;
  }

  setPath(option) {
    let loggedInUser = JSON.parse(localStorage.getItem('currentUser'));
    let cust = localStorage.getItem('selected-customer-detail');
    if(cust != undefined && cust != "null") {
      localStorage.setItem('navigationPath', '');  
      if(option == 'itemHistory')
        this.router.navigate(['/item-history']);
      else{
        this.router.navigate(['/favourites']);
      }
    }  
    else if(loggedInUser != undefined && loggedInUser != "null"){
      if (loggedInUser.UserTypeId == 1001){
        if(option == 'itemHistory')
          localStorage.setItem('navigationPath', '/item-history');  
        else{
          localStorage.setItem('navigationPath', '/favourites');  
        }
        this.router.navigate(['/search-customer']);
      }
      else{
        if(option == 'itemHistory')
          this.router.navigate(['/item-history']);
        else{
          this.router.navigate(['/favourites']);
        }
      }
    }
    else{
      this.router.navigate(['/login']);
    }
  }
}
