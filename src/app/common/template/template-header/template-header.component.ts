import { Component, OnInit } from '@angular/core';
import { ApplicationService } from "../../application.service";
import { Router } from "@angular/router";
import { AppComponent } from "../../../components/app.component";
import { User } from "../../../models/user";

@Component({
  selector: 'app-template-header',
  templateUrl: './template-header.component.html',
  styleUrls: ['./template-header.component.css']

})
export class TemplateHeaderComponent implements OnInit {
  isLoggedIn: boolean = false;
  clicked = false;
  currentUser: User;
  selectedCust: any;
  globalSearchText: string = "";

  constructor(private _appService: ApplicationService,
              private router: Router,
              private appComponent: AppComponent) { 
              this.selectedCust = this._appService.selectedCust;
              }

  ngOnInit() {
    this.isUserLoggedIn();
    this.currentUser = this._appService.currentUser;
    
    this._appService.notifyObservable$.subscribe((response: any) => {
      if(response && response.data == "true"){
        this.logOut();
      }
    });
  }

//   someFunc($event){
//     if($event.which == 13) {
//       this.globalSearch();
//     }
// }

  globalSearch(){
    localStorage.setItem('globalSearchText', this.globalSearchText);
    this.router.navigate(['/home']).then(()=>{this.router.navigate(['/item-search'])})
  }

  setPath() {
    let loggedInUser = JSON.parse(localStorage.getItem('currentUser'));
    let cust = localStorage.getItem('selected-customer-detail');
    if(cust != undefined && cust != "null") {
      localStorage.setItem('navigationPath', '');  
      this.router.navigate(['/service-request']);
    }  
    else if(loggedInUser != undefined && loggedInUser != "null"){
      if (loggedInUser.UserTypeId == 1001){
        localStorage.setItem('navigationPath', '/service-request');  
        this.router.navigate(['/search-customer']);
      }
      else{
        localStorage.setItem('navigationPath', '');  
        this.router.navigate(['/service-request']);
      }
    }
    else{
      this.router.navigate(['/login']);
    }
  }

  toggleMenu() {
    this.clicked = !this.clicked;
    this._appService.notifyChild({ data: this.clicked });
  }

  logOut() {
    this._appService.logoutUser().subscribe(data => {
      if (data.Success) {
        localStorage.setItem('currentUser', null);
        //localStorage.setItem('selected-customer-detail', null);
       localStorage.isUserTimedout = "false";
        localStorage.clear();
        this._appService.currentUser = null;
        this._appService.selectedCust = null;
        this._appService.setUserLoggedIn(false);
        // this.isLoggedIn = false;
        this.appComponent.isUserLoggedIn();   
        this.appComponent.isCustSelected = false;   
        this.router.navigate(["/login"]);
      } else {
        localStorage.clear();
        this._appService.currentUser = null;
        this._appService.setUserLoggedIn(false);
        this.appComponent.isUserLoggedIn();
        this.router.navigate(['/login']);
      }
    });
  }

  isUserLoggedIn(): void {

    this.isLoggedIn = this._appService.getUserLoggedIn();
    if (this._appService.currentUser) {
      this.isLoggedIn = true;
    }
    else {
      this.isLoggedIn = false;
    }
  }
}

