import { Component, OnInit } from '@angular/core';
import { FooterDetail } from "../../../models/footer-detail";
import { ApplicationService } from "../../application.service";

@Component({
  selector: 'app-template-footer',
  templateUrl: './template-footer.component.html',
  styleUrls: ['./template-footer.component.css']
})
export class TemplateFooterComponent implements OnInit {

  footerData : FooterDetail = new FooterDetail();
  
  isUser : boolean=true;
  clicked = false;
  
  constructor(private _appService: ApplicationService) {
    this.getFooterDetail();
    }

  ngOnInit() {
    let currentuser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentuser!=null){
      if(currentuser.UserType === "Customer"){
        this.isUser=false;
       }else{
        this.isUser=true;
       }
     }
  }
  getFooterDetail() {
    return this._appService.getFooterDetail().subscribe(data => {
      this.footerData = data.Content.Result;
    });
  }
}
