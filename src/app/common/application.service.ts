import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Subject }    from 'rxjs/Subject';
import { User } from '../models/user';
import { HttpService } from "./http.service";
import { Response, Http, Headers } from "@angular/http";
import { Router } from "@angular/router";
import {environment} from '../../environments/environment';

@Injectable()
export class ApplicationService {
    public callbackUrl='/home';
    public isUserLoggedIn;
    currentUser: any;
    selectedCust:any;
    private notify = new Subject<any>();
    notifyObservable$ = this.notify.asObservable();

    constructor(private _http: Http,private _httpService: HttpService,private router : Router) {
        this.isUserLoggedIn = false;
        const currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser != null) {
          this.currentUser = currentUser;
        }
        const selectedCust :any = JSON.parse(localStorage.getItem('selected-customer-detail'));
        if(selectedCust != null){
            this.selectedCust=selectedCust;
        }
    }
    
    public setUserLoggedIn(value : boolean) {
        this.isUserLoggedIn = value;
    }

    public getUserLoggedIn() {
        return this.isUserLoggedIn;
    }
    
    public getFooterDetail() : Observable <any> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this._httpService.get('FooterDetail', {headers: headers})
               .map((res : Response) => res.json())
               .catch(this.handleError);
    }

    public logoutUser() {
        localStorage.isUserTimedout = "false";  
        return this._httpService.get('LogOut').map(res =>{
            if (res.status != 200) {
                throw new Error('No Data to retrieve! code status ' + res.status);
            } 
            else {
                return JSON.parse(res["_body"]);
            }
        })
        .catch(this.handleError);                
    }

    public notifyChild(data: any) {
        if (data) {
            this.notify.next(data);
            return this._httpService.get('LogOut').map(res =>{
                if (res.status != 200) {
                    throw new Error('No Data to retrieve! code status ' + res.status);
                } 
                else {
                    return JSON.parse(res["_body"]);
                }
            })
            .catch(this.handleError);                              
        }
    }

    public navigate(currentPath : string, navigatePath : string){
      localStorage.setItem('callback-url',navigatePath);
      this.callbackUrl=currentPath;
      this.router.navigate(['/'+navigatePath]);
    }

    private handleError(error: Response) {
        return Observable.throw('Server error');
    }
}