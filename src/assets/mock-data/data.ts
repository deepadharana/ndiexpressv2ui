import { NewItemHistory } from "./newitem-detail";

export const PersonMain : NewItemHistory[]=[
    {
        Item: "A",
        ItemId: 100,
        Selected: false,
        Desc: "Gloves fege xx",
        Brand: "ABC",
        Order: "2",
        Date: new Date("February 4, 2016 10:13:00"),
        Quantity: 1,
        Avalibility: "May be ordered.",
        Price: "2500.00",
        IsVisible: true,

        // item-detail values

        Category : "Flosser",
        YourPrice : 3200.00,
        YouSaved : 800.00,
        PromotionDescription : "Buy 1, Get Trimax Instrument Demo Kit FREE",
        ItemImage : "./assets/img/item-detail/main-image.jpg",
        SubstituteImage : "./assets/img/item-detail/small-image.jpg",
        AlternateImage : "./assets/img/item-detail/small-image.jpg",
        InitialQuantity : 0
        

    },
]

export const Persons: NewItemHistory[] = [
    {
        Item: "A",
        ItemId: 1,
        Selected: false,
        Desc: "Gloves fege xx",
        Brand: "ABC",
        Order: "2",
        Date: new Date("February 4, 2016 10:13:00"),
        Quantity: 1,
        Avalibility: "May be ordered.",
        Price: "2500.00",
        IsVisible: true,

        // item-detail values

        Category : "Flosser",
        YourPrice : 3200.00,
        YouSaved : 800.00,
        PromotionDescription : "Buy 1, Get Trimax Instrument Demo Kit FREE",
        ItemImage : "./assets/img/item-detail/main-image.jpg",
        SubstituteImage : "./assets/img/item-detail/small-image.jpg",
        AlternateImage : "./assets/img/item-detail/small-image.jpg",
        InitialQuantity : 0
        

    },
    {
        Item: "Z",
        ItemId: 2,
        Selected: false,
        Desc: "Sfsfs Gloves fege",
        Brand: "DEF",
        Order: "5",
        Date: new Date("February 9, 2016 10:13:00"),
        Quantity: 5,
        Avalibility: "May be ordered.",
        Price: "12500.00",
        IsVisible: true,

        // item-detail values

        Category : "Flosser",
        YourPrice : 3200.00,
        YouSaved : 800.00,
        PromotionDescription : "Buy 1, Get Trimax Instrument Demo Kit FREE",
        ItemImage : "./assets/img/item-detail/main-image.jpg",
        SubstituteImage : "./assets/img/item-detail/small-image.jpg",
        AlternateImage : "./assets/img/item-detail/small-image.jpg",
        InitialQuantity : 0
    },
    {
        Item: "C",
        ItemId: 3,
        Selected: false,
        Desc: "Sfsfs Gloves fege",
        Brand: "A",
        Order: "5",
        Date: new Date("February 23, 2016 10:13:00"),
        Quantity: 10,
        Avalibility: "May be ordered.",
        Price: "12500.00",
        IsVisible: true,

        // item-detail values

        Category : "Flosser",
        YourPrice : 3200.00,
        YouSaved : 800.00,
        PromotionDescription : "Buy 1, Get Trimax Instrument Demo Kit FREE",
        ItemImage : "./assets/img/item-detail/main-image.jpg",
        SubstituteImage : "./assets/img/item-detail/small-image.jpg",
        AlternateImage : "./assets/img/item-detail/small-image.jpg",
        InitialQuantity : 0
    },
    {
        Item: "E",
        ItemId: 4,
        Selected: false,
        Desc: "Sfsfs Gloves fege",
        Brand: "JKL",
        Order: "5",
        Date: new Date("February 4, 2016 10:11:00"),
        Quantity: 10,
        Avalibility: "May be ordered.",
        Price: "12500.00",
        IsVisible: true,

        // item-detail values

        Category : "Flosser",
        YourPrice : 3200.00,
        YouSaved : 800.00,
        PromotionDescription : "Buy 1, Get Trimax Instrument Demo Kit FREE",
        ItemImage : "./assets/img/item-detail/main-image.jpg",
        SubstituteImage : "./assets/img/item-detail/small-image.jpg",
        AlternateImage : "./assets/img/item-detail/small-image.jpg",
        InitialQuantity : 0
    },
    {
        Item: "D",
        ItemId: 5,
        Selected: false,
        Desc: "Sfsfs Gloves fege",
        Brand: "L",
        Order: "5",
        Date: new Date("February 4, 2016 10:13:00"),
        Quantity: 6,
        Avalibility: "May be ordered.",
        Price: "12500.00",
        IsVisible: true,

        // item-detail values

        Category : "Flosser",
        YourPrice : 3200.00,
        YouSaved : 800.00,
        PromotionDescription : "Buy 1, Get Trimax Instrument Demo Kit FREE",
        ItemImage : "./assets/img/item-detail/main-image.jpg",
        SubstituteImage : "./assets/img/item-detail/small-image.jpg",
        AlternateImage : "./assets/img/item-detail/small-image.jpg",
        InitialQuantity : 0
    }
    
]

export const Persons2 : NewItemHistory[]=[

    {
        Selected: false,
        Item: "c",
        ItemId: 6,
        Desc: "Sfsfs Gloves fege",
        Brand: "PQR",
        Order: "5",
        Date: new Date("February 1, 2016 10:13:00"),
        Quantity: 3,
        Avalibility: "May be ordered.",
        Price: "12500.00",
        IsVisible: true,

        // item-detail values

        Category : "Flosser",
        YourPrice : 3200.00,
        YouSaved : 800.00,
        PromotionDescription : "Buy 1, Get Trimax Instrument Demo Kit FREE",
        ItemImage : "./assets/img/item-detail/main-image.jpg",
        SubstituteImage : "./assets/img/item-detail/small-image.jpg",
        AlternateImage : "./assets/img/item-detail/small-image.jpg",
        InitialQuantity : 0
    },
    {
        Item: "B",
        ItemId: 7,
        Selected: false,
        Desc: "Sfsfs Gloves fege",
        Brand: "STU",
        Order: "5",
        Date: new Date("February 28, 2016 10:13:00"),
        Quantity: 22,
        Avalibility: "May be ordered.",
        Price: "12500.00",
        IsVisible: true,

        // item-detail values

        Category : "Flosser",
        YourPrice : 3200.00,
        YouSaved : 800.00,
        PromotionDescription : "Buy 1, Get Trimax Instrument Demo Kit FREE",
        ItemImage : "./assets/img/item-detail/main-image.jpg",
        SubstituteImage : "./assets/img/item-detail/small-image.jpg",
        AlternateImage : "./assets/img/item-detail/small-image.jpg",
        InitialQuantity : 0
    },
    {
        Item: "R",
        ItemId: 8,
        Selected: false,
        Desc: "Sfsfs Gloves fege",
        Brand: "VWX",
        Order: "5",
        Date: new Date("February 14, 2016 10:13:00"),
        Quantity: 8,
        Avalibility: "May be ordered.",
        Price: "12500.00",
        IsVisible: true,

        // item-detail values

        Category : "Flosser",
        YourPrice : 3200.00,
        YouSaved : 800.00,
        PromotionDescription : "Buy 1, Get Trimax Instrument Demo Kit FREE",
        ItemImage : "./assets/img/item-detail/main-image.jpg",
        SubstituteImage : "./assets/img/item-detail/small-image.jpg",
        AlternateImage : "./assets/img/item-detail/small-image.jpg",
        InitialQuantity : 0
    },
    {
        Item: "D",
        ItemId: 9,
        Selected: false,
        Desc: "Sfsfs Gloves fege",
        Brand: "YZ",
        Order: "5",
        Date: new Date("February 24, 2016 10:13:00"),
        Quantity: 56,
        Avalibility: "May be ordered.",
        Price: "12500.00",
        IsVisible: true,

        // item-detail values

        Category : "Flosser",
        YourPrice : 3200.00,
        YouSaved : 800.00,
        PromotionDescription : "Buy 1, Get Trimax Instrument Demo Kit FREE",
        ItemImage : "./assets/img/item-detail/main-image.jpg",
        SubstituteImage : "./assets/img/item-detail/small-image.jpg",
        AlternateImage : "./assets/img/item-detail/small-image.jpg",
        InitialQuantity : 0
    },
    {
        Item: "X",
        ItemId: 10,
        Selected: false,
        Desc: "Sfsfs Gloves fege",
        Brand: "ABCD",
        Order: "5",
        Date: new Date("February 3, 2016 10:13:00"),
        Quantity: 12,
        Avalibility: "May be ordered.",
        Price: "12500.00",
        IsVisible: true,

        // item-detail values

        Category : "Flosser",
        YourPrice : 3200.00,
        YouSaved : 800.00,
        PromotionDescription : "Buy 1, Get Trimax Instrument Demo Kit FREE",
        ItemImage : "./assets/img/item-detail/main-image.jpg",
        SubstituteImage : "./assets/img/item-detail/small-image.jpg",
        AlternateImage : "./assets/img/item-detail/small-image.jpg",
        InitialQuantity : 0
    },
    {
        Item: "P",
        ItemId: 11,
        Selected: false,
        Desc: "Sfsfs Gloves fege",
        Brand: "EFGH",
        Order: "5",
        Date: new Date("February 11, 2016 10:13:00"),
        Quantity: 2,
        Avalibility: "May be ordered.",
        Price: "12500.00",
        IsVisible: true,

        // item-detail values

        Category : "Flosser",
        YourPrice : 3200.00,
        YouSaved : 800.00,
        PromotionDescription : "Buy 1, Get Trimax Instrument Demo Kit FREE",
        ItemImage : "./assets/img/item-detail/main-image.jpg",
        SubstituteImage : "./assets/img/item-detail/small-image.jpg",
        AlternateImage : "./assets/img/item-detail/small-image.jpg",
        InitialQuantity : 0
    },
    {
        Item: "G",
        ItemId: 12,
        Selected: false,
        Desc: "Sfsfs Gloves fege",
        Brand: "IJKL    ",
        Order: "5",
        Date: new Date("February 20, 2016 10:13:00"),
        Quantity: 9,
        Avalibility: "May be ordered.",
        Price: "12500.00",
        IsVisible: true,

        // item-detail values

        Category : "Flosser",
        YourPrice : 3200.00,
        YouSaved : 800.00,
        PromotionDescription : "Buy 1, Get Trimax Instrument Demo Kit FREE",
        ItemImage : "./assets/img/item-detail/main-image.jpg",
        SubstituteImage : "./assets/img/item-detail/small-image.jpg",
        AlternateImage : "./assets/img/item-detail/small-image.jpg",
        InitialQuantity : 0
    }
]

export const Avaibility = ["avaibility1", "avaibility2", "avaibility3", "avaibility4", "avaibility5"]

export const Brand = ["brand1", "brand2", "brand3", "brand4", "brand5"]

export const Categories = ["category1", "category2", "category3", "category4", "category5"]