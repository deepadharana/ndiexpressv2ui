export class NewItemHistory{
    Selected: boolean;
    Item: string;
    ItemId: number;
    Desc: string;
    Brand: string;
    Order: string;
    Date: Date;
    Quantity: number;
    Avalibility: string;
    Price: string;
    IsVisible: boolean;
  
  
  
  
    // Item detail fields
    Category : string;
    YourPrice : number;
    YouSaved : number;
    PromotionDescription : string;
    ItemImage : string;
    SubstituteImage : string;
    AlternateImage : string;
    InitialQuantity : number;
  }