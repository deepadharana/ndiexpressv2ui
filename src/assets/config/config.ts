export const Configuration = {
    "system": {
        "applicationName": "NDI Express V2",
        "applicationUrl": "https://ndiexpress.compunnel.com/API/"
    },
    "seo": {
        "pageTitle": "NDI Express V2"
    },
    "i18n":{
        "locale": "en"
    }
}